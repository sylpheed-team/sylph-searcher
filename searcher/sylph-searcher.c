/* Sylph-Searcher - full-text search program for Sylpheed
 * Copyright (C) 2007-2009 Sylpheed Development Team
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <sylph/sylmain.h>
#include <sylph/prefs_common.h>
#include <sylph/account.h>
#include <sylph/folder.h>
#include <sylph/procmsg.h>
#include <sylph/codeconv.h>
#include <libpq-fe.h>
#include <stdlib.h>
#include <time.h>

#if HAVE_LOCALE_H
#  include <locale.h>
#endif

#ifdef G_OS_WIN32
#  include <windows.h>
#  include <winreg.h>
#  include <wchar.h>
#  include <shlobj.h>
#  include <fcntl.h>
#endif

#include "version.h"
#include "common.h"
#include "searcher-config.h"
#include "folder-selector.h"

#define PG_CONN(dbinfo) ((PGconn *)dbinfo->dbdata)

enum
{
	COL_SUBJECT,
	COL_FROM,
	COL_DATE,
	COL_FOLDER,
	COL_MSGNUM,
	COL_MSGSID,
	N_COLS
};

typedef struct _QueryInfo {
	gchar *body;
	gchar *from;
	gchar *to;
	gchar *subject;
	gchar *date_gt;
	gchar *date_lt;
	gchar *folder;
	gboolean folder_with_sub;
	gboolean has_attach;

	gint res_limit;
} QueryInfo;

static AppConfig config;

DBInfo *conn;
QueryInfo *query_info;

static struct SylFTSearchWindow {
	GtkWidget *window;

	GtkWidget *body_entry;

	GtkWidget *from_entry;
	GtkWidget *to_entry;
	GtkWidget *subject_entry;
	GtkWidget *dategt_entry;
	GtkWidget *datelt_entry;
	GtkWidget *folder_entry;
	GtkWidget *folder_btn;
	GtkWidget *folder_with_sub_chkbtn;
	GtkWidget *has_attach_chkbtn;

	GtkWidget *treeview;
	GtkListStore *store;

	GtkWidget *textview;
	GtkTextTag *tag;

	GtkWidget *status_label;

	GtkWidget *search_btn;
	GtkWidget *clear_btn;
	GtkWidget *close_btn;
} ftsearch_window;

const gchar *ui_def =
	"<ui>"
	"  <menubar name='MainMenu'>"
	"    <menu name='File' action='FileAction'>"
	"      <menuitem name='Exit' action='ExitAction'/>"
	"    </menu>"
	"    <menu name='Edit' action='EditAction'>"
	"      <menuitem name='Copy' action='CopyAction'/>"
	"      <menuitem name='Paste' action='PasteAction'/>"
	"    </menu>"
	"    <menu name='Tools' action='ToolsAction'>"
	"      <menuitem name='Settings' action='SettingsAction'/>"
	"    </menu>"
	"    <menu name='Help' action='HelpAction'>"
	"      <menuitem name='About' action='AboutAction'/>"
	"    </menu>"
	"  </menubar>"
	"</ui>";

static gint db_query(const QueryInfo *qinfo);

static void update_text(const gchar *text);

static QueryInfo *query_info_new(const gchar *body, const gchar *from,
				 const gchar *to, const gchar *subject);
static void query_info_free(QueryInfo *qinfo);

static void row_activated	(GtkTreeView		*treeview,
				 GtkTreePath		*path,
				 GtkTreeViewColumn	*column,
				 gpointer		 data);
static void selection_changed	(GtkTreeSelection	*selection,
				 gpointer		 data);

static void search_entry_activated	(GtkWidget	*widget,
					 gpointer	 data);

static gint search_deleted	(GtkWidget	*widget,
				 GdkEventAny	*event,
				 gpointer	 data);

static void folder_btn_clicked	(GtkButton	*button,
				 gpointer	 data);
static void clear_clicked	(GtkButton	*button,
				 gpointer	 data);
static void search_clicked	(GtkButton	*button,
				 gpointer	 data);
static void close_clicked	(GtkButton	*button,
				 gpointer	 data);

static void exit_cb	(void);
static void copy_cb	(void);
static void paste_cb	(void);
static void settings_cb	(void);
static void about_cb	(void);

static GtkActionEntry action_entries[] = {
	{"FileAction", NULL, N_("_File"), NULL, NULL, NULL},
	{"ExitAction", NULL, N_("E_xit"), NULL, NULL, exit_cb},
	{"EditAction", NULL, N_("_Edit"), NULL, NULL, NULL},
	{"CopyAction", NULL, N_("_Copy"), NULL, NULL, copy_cb},
	{"PasteAction", NULL, N_("_Paste"), NULL, NULL, paste_cb},
	{"ToolsAction", NULL, N_("_Tools"), NULL, NULL, NULL},
	{"SettingsAction", NULL, N_("_Settings"), NULL, NULL, settings_cb},
	{"HelpAction", NULL, N_("_Help"), NULL, NULL, NULL},
	{"AboutAction", NULL, N_("_About"), NULL, NULL, about_cb},
};

static gint db_query(const QueryInfo *qinfo)
{
	PGresult *res;
	GString *query;
	gboolean have_cond = FALSE;
	gint i;
	gchar *val;
	gchar *esc_query_str;
	gulong sid;
	gchar *from, *to, *subject, *msg_date, *folder_id;
	guint msgnum;
	GtkListStore *store = ftsearch_window.store;
	GtkTreeIter iter;
	gchar *msg;

	g_return_val_if_fail(conn != NULL, -1);

	gtk_list_store_clear(store);
	update_text("");
	gtk_label_set_text(GTK_LABEL(ftsearch_window.status_label), "");
	query_info_free(query_info);
	query_info = NULL;

	if (db_check_connection(conn) < 0) {
		gtk_label_set_text(GTK_LABEL(ftsearch_window.status_label),
				   _("Database connection error"));
		return -1;
	}

	query = g_string_new("SELECT msg_sid, hdr_from, hdr_to, hdr_subject, msg_date, folder_id, msgnum FROM msginfo LEFT JOIN msg_folderinfo USING (msg_sid) WHERE ");

	if (qinfo->body) {
		gchar *sep_query_str;
		GSList *phrase_text = NULL, *cur;

		sep_query_str = get_wakachi_text(qinfo->body);
		esc_query_str = sql_escape_str(conn, sep_query_str);
		g_string_append_printf
			(query, "plainto_tsquery(E'%s') @@ body_index",
			 esc_query_str);
		g_free(esc_query_str);
		g_free(sep_query_str);
		have_cond = TRUE;

		phrase_text = get_phrase_text(qinfo->body);
		if (phrase_text) {
			for (cur = phrase_text; cur != NULL; cur = cur->next) {
				esc_query_str = sql_escape_like_str
					(conn, (gchar *)cur->data);
				g_string_append_printf(query, " AND body_text ILIKE E'%%%s%%'", esc_query_str);
				g_free(esc_query_str);
				g_free(cur->data);
			}
			g_slist_free(phrase_text);
		}
	}

#define MAKE_COND(header)						\
	if (qinfo->header) {						\
		esc_query_str = sql_escape_like_str(conn, qinfo->header); \
		g_string_append_printf(query, "%shdr_" # header		\
				       " ILIKE E'%%%s%%'",		\
				       have_cond ? " AND " : "",	\
				       esc_query_str);			\
		g_free(esc_query_str);					\
		have_cond = TRUE;					\
	}

	MAKE_COND(from);

	if (qinfo->to) {
		esc_query_str = sql_escape_like_str(conn, qinfo->to);
		g_string_append_printf(query, "%s(hdr_to ILIKE E'%%%s%%'"
				       " OR hdr_cc ILIKE E'%%%s%%')",
				       have_cond ? " AND " : "",
				       esc_query_str, esc_query_str);
		g_free(esc_query_str);
		have_cond = TRUE;
	}

	MAKE_COND(subject);

	if (qinfo->date_gt) {
		esc_query_str = sql_escape_str(conn, qinfo->date_gt);
		g_string_append_printf(query, "%smsg_date > E'%s'",
				       have_cond ? " AND " : "",
				       esc_query_str);
		g_free(esc_query_str);
		have_cond = TRUE;
	}
	if (qinfo->date_lt) {
		esc_query_str = sql_escape_str(conn, qinfo->date_lt);
		g_string_append_printf(query, "%smsg_date < E'%s'",
				       have_cond ? " AND " : "",
				       esc_query_str);
		g_free(esc_query_str);
		have_cond = TRUE;
	}

	if (qinfo->folder) {
		esc_query_str = sql_escape_str(conn, qinfo->folder);
		if (qinfo->folder_with_sub) {
			g_string_append_printf
				(query, "%s(folder_id = E'%s' OR folder_id LIKE E'%s/%%')",
				 have_cond ? " AND " : "", esc_query_str, esc_query_str);
		} else {
			g_string_append_printf
				(query, "%sfolder_id = E'%s'",
				 have_cond ? " AND " : "", esc_query_str);
		}
		g_free(esc_query_str);
		have_cond = TRUE;
	}
	if (qinfo->has_attach && have_cond) {
		g_string_append_printf(query, " AND attach_num > 0");
	}

	if (!have_cond) {
		g_string_free(query, TRUE);
		return -1;
	}

	if (qinfo->res_limit > 0) {
		g_string_prepend(query, "SELECT * FROM (");
		g_string_append_printf
			(query, " ORDER BY msg_sid DESC LIMIT %d) AS m"
				" ORDER BY msg_sid ASC", qinfo->res_limit);
	}

	g_print("query: %s\n", query->str);
	res = PQexec(PG_CONN(conn), query->str);
	g_string_free(query, TRUE);

	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		db_error_message(conn, "SELECT failed");
		PQclear(res);
		if (db_check_connection(conn) < 0) {
			gtk_label_set_text
				(GTK_LABEL(ftsearch_window.status_label),
				 _("Database connection error"));
		} else {
			gtk_label_set_text
				(GTK_LABEL(ftsearch_window.status_label),
				 _("Database query error"));
		}
		return -1;
	}

	msg = g_strdup_printf(_("%d messages found."), PQntuples(res));
	g_print("%s\n\n", msg);
	gtk_label_set_text(GTK_LABEL(ftsearch_window.status_label), msg);

	for (i = 0; i < PQntuples(res); i++) {
		val = PQgetvalue(res, i, 0);
		sid = strtoul(val, NULL, 0);
		from = PQgetvalue(res, i, 1);
		to = PQgetvalue(res, i, 2);
		subject = PQgetvalue(res, i, 3);
		msg_date = PQgetvalue(res, i, 4);
		folder_id = PQgetvalue(res, i, 5);
		val = PQgetvalue(res, i, 6);
		msgnum = strtoul(val, NULL, 0);

		//g_print("%lu %s %s %s %s %s %u\n", sid, from, to, subject, msg_date, folder_id, msgnum);

		gtk_list_store_append(store, &iter);
		gtk_list_store_set(store, &iter,
				   COL_SUBJECT, subject,
				   COL_FROM, from,
				   COL_DATE, msg_date,
				   COL_FOLDER, folder_id,
				   COL_MSGNUM, msgnum,
				   COL_MSGSID, sid,
				   -1);
	}

	PQclear(res);

	query_info = query_info_new(qinfo->body, qinfo->from, qinfo->to,
				    qinfo->subject);

	return 0;
}

static gboolean gtkut_text_buffer_match_string(GtkTextBuffer *textbuf,
					       const GtkTextIter *iter,
					       gunichar *wcs, gint len,
					       gboolean case_sens)
{
	GtkTextIter start_iter, end_iter;
	gchar *utf8str, *p;
	gint match_count;

	start_iter = end_iter = *iter;
	gtk_text_iter_forward_chars(&end_iter, len);

	utf8str = gtk_text_buffer_get_text(textbuf, &start_iter, &end_iter,
					   FALSE);
	if (!utf8str) return FALSE;

	if ((gint)g_utf8_strlen(utf8str, -1) != len) {
		g_free(utf8str);
		return FALSE;
	}

	for (p = utf8str, match_count = 0; *p != '\0' && match_count < len;
	     p = g_utf8_next_char(p), match_count++) {
		gunichar wc;

		wc = g_utf8_get_char(p);

		if (case_sens) {
			if (wc != wcs[match_count])
				break;
		} else {
			if (g_unichar_tolower(wc) !=
			    g_unichar_tolower(wcs[match_count]))
				break;
		}
	}

	g_free(utf8str);

	if (match_count == len)
		return TRUE;
	else
		return FALSE;
}

static gboolean gtkut_text_buffer_find(GtkTextBuffer *buffer,
				       const GtkTextIter *iter,
				       const gchar *str, gboolean case_sens,
				       GtkTextIter *match_pos)
{
	gunichar *wcs;
	gint len;
	glong items_read = 0, items_written = 0;
	GError *error = NULL;
	GtkTextIter iter_;
	gboolean found = FALSE;

	wcs = g_utf8_to_ucs4(str, -1, &items_read, &items_written, &error);
	if (error != NULL) {
		g_warning("An error occured while converting a string from UTF-8 to UCS-4: %s\n", error->message);
		g_error_free(error);
	}
	if (!wcs || items_written <= 0) return FALSE;
	len = (gint)items_written;

	iter_ = *iter;
	do {
		found = gtkut_text_buffer_match_string
			(buffer, &iter_, wcs, len, case_sens);
		if (found) {
			*match_pos = iter_;
			break;
		}
	} while (gtk_text_iter_forward_char(&iter_));

	g_free(wcs);

	return found;
}

static void update_text(const gchar *text)
{
	GtkTextBuffer *buffer;
	GtkTextIter iter, pos;
	GtkTextMark *mark;
	gchar *sep_str;
	gchar **keywords;
	gint i, len;

	if (!text)
		text = "";

	buffer = gtk_text_view_get_buffer
		(GTK_TEXT_VIEW(ftsearch_window.textview));
	gtk_text_buffer_set_text(buffer, text, -1);
	gtk_text_buffer_get_start_iter(buffer, &iter);
	gtk_text_buffer_place_cursor(buffer, &iter);
	mark = gtk_text_buffer_get_insert(buffer);
	gtk_text_view_scroll_mark_onscreen
		(GTK_TEXT_VIEW(ftsearch_window.textview), mark);

	if (!query_info || !query_info->body)
		return;

	sep_str = get_wakachi_text(query_info->body);
	keywords = g_strsplit(sep_str, " ", 0);

	for (i = 0; keywords[i] != NULL; i++) {
		gtk_text_buffer_get_start_iter(buffer, &iter);
		len = g_utf8_strlen(keywords[i], -1);
		while (gtkut_text_buffer_find(buffer, &iter, keywords[i], FALSE,
					      &pos)) {
			GtkTextIter end = pos;

			gtk_text_iter_forward_chars(&end, len);
			gtk_text_buffer_apply_tag(buffer, ftsearch_window.tag,
						  &pos, &end);
			iter = end;
		}
	}

	g_strfreev(keywords);
	g_free(sep_str);
}

static QueryInfo *query_info_new(const gchar *body, const gchar *from,
				 const gchar *to, const gchar *subject)
{
	QueryInfo *qinfo;

	qinfo = g_new(QueryInfo, 1);
	qinfo->body = g_strdup(body);
	qinfo->from = g_strdup(from);
	qinfo->to = g_strdup(to);
	qinfo->subject = g_strdup(subject);

	return qinfo;
}

static void query_info_free(QueryInfo *qinfo)
{
	if (!qinfo)
		return;

	g_free(qinfo->body);
	g_free(qinfo->from);
	g_free(qinfo->to);
	g_free(qinfo->subject);
	g_free(qinfo);
}

static void syl_searcher_create(void)
{
	GtkWidget *window;
	GtkWidget *vbox_top;
	GtkWidget *vbox;

	GtkActionGroup *group;
	GtkUIManager *ui;
	GtkWidget *menubar;

	GtkWidget *table;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *body_entry;
	GtkWidget *from_entry;
	GtkWidget *to_entry;
	GtkWidget *subject_entry;
	GtkWidget *dategt_entry;
	GtkWidget *datelt_entry;
	GtkTooltips *tip;
	GtkWidget *folder_entry;
	GtkWidget *folder_btn;
	GtkWidget *folder_with_sub_chkbtn;
	GtkWidget *has_attach_chkbtn;

	GtkWidget *vpaned;

	GtkWidget *scrolledwin;

	GtkWidget *treeview;
	GtkListStore *store;
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;
	GtkTreeSelection *selection;

	GtkWidget *textview;
	GtkTextBuffer *buffer;
	GtkTextTag *tag;

	GtkWidget *status_label;

	GtkWidget *hbbox;
	GtkWidget *search_btn;
	GtkWidget *clear_btn;
	GtkWidget *close_btn;

	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW (window), _("Search messages"));
	gtk_widget_set_size_request(window, 620, -1);
	gtk_window_set_policy(GTK_WINDOW(window), FALSE, TRUE, TRUE);
	g_signal_connect(G_OBJECT(window), "delete_event",
			 G_CALLBACK(search_deleted), NULL);

	vbox_top = gtk_vbox_new(FALSE, 4);
	gtk_container_add(GTK_CONTAINER(window), vbox_top);

	group = gtk_action_group_new("main");
	gtk_action_group_set_translation_domain(group, textdomain(NULL));
	gtk_action_group_add_actions(group, action_entries,
				     sizeof(action_entries) /
				     sizeof(action_entries[0]), NULL);

	ui = gtk_ui_manager_new();
	gtk_ui_manager_insert_action_group(ui, group, 0);
	gtk_ui_manager_add_ui_from_string(ui, ui_def, -1, NULL);
	menubar = gtk_ui_manager_get_widget(ui, "/MainMenu");
	gtk_box_pack_start(GTK_BOX(vbox_top), menubar, FALSE, FALSE, 0);

	vbox = gtk_vbox_new(FALSE, 6);
	gtk_box_pack_start(GTK_BOX(vbox_top), vbox, TRUE, TRUE, 0);
	gtk_container_set_border_width(GTK_CONTAINER (vbox), 8);

	table = gtk_table_new(5, 2, FALSE);
	gtk_table_set_col_spacings(GTK_TABLE(table), 4);
	gtk_table_set_row_spacings(GTK_TABLE(table), 4);
	gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, 0);

	hbox = gtk_hbox_new(FALSE, 8);
	label = gtk_label_new(_("Body:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 0, 1, GTK_FILL, 0, 0, 0);

	body_entry = gtk_entry_new();
	gtk_table_attach_defaults(GTK_TABLE(table), body_entry, 1, 2, 0, 1);
	g_signal_connect(G_OBJECT(body_entry), "activate",
			 G_CALLBACK(search_entry_activated), NULL);
	tip = gtk_tooltips_new();
	gtk_tooltips_set_tip(tip, body_entry,
			     _("Enter words or sentence included in body text\n"
			       "(Sentence is automatically divided into words)"),
			     NULL);

	hbox = gtk_hbox_new(FALSE, 8);
	label = gtk_label_new(_("From:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 1, 2, GTK_FILL, 0, 0, 0);

	from_entry = gtk_entry_new();
	gtk_table_attach_defaults(GTK_TABLE(table), from_entry, 1, 2, 1, 2);
	g_signal_connect(G_OBJECT(from_entry), "activate",
			 G_CALLBACK(search_entry_activated), NULL);
	tip = gtk_tooltips_new();
	gtk_tooltips_set_tip(tip, from_entry,
			     _("Specify part of name or address of sender (From)"),
			     NULL);

	hbox = gtk_hbox_new(FALSE, 8);
	label = gtk_label_new(_("To:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 2, 3, GTK_FILL, 0, 0, 0);

	to_entry = gtk_entry_new();
	gtk_table_attach_defaults(GTK_TABLE(table), to_entry, 1, 2, 2, 3);
	g_signal_connect(G_OBJECT(to_entry), "activate",
			 G_CALLBACK(search_entry_activated), NULL);
	tip = gtk_tooltips_new();
	gtk_tooltips_set_tip(tip, to_entry,
			     _("Specify part of name or address of recipient (To or Cc)"),
			     NULL);

	hbox = gtk_hbox_new(FALSE, 8);
	label = gtk_label_new(_("Subject:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 3, 4, GTK_FILL, 0, 0, 0);

	subject_entry = gtk_entry_new();
	gtk_table_attach_defaults(GTK_TABLE(table), subject_entry, 1, 2, 3, 4);
	g_signal_connect(G_OBJECT(subject_entry), "activate",
			 G_CALLBACK(search_entry_activated), NULL);
	tip = gtk_tooltips_new();
	gtk_tooltips_set_tip(tip, subject_entry, _("Specify part of subject"),
			     NULL);

	hbox = gtk_hbox_new(FALSE, 8);
	label = gtk_label_new(_("Folder:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 4, 5, GTK_FILL, 0, 0, 0);

	hbox = gtk_hbox_new(FALSE, 8);
	gtk_table_attach_defaults(GTK_TABLE(table), hbox, 1, 2, 4, 5);

	folder_entry = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(hbox), folder_entry, TRUE, TRUE, 0);
	g_signal_connect(G_OBJECT(folder_entry), "activate",
			 G_CALLBACK(search_entry_activated), NULL);
	tip = gtk_tooltips_new();
	gtk_tooltips_set_tip(tip, folder_entry, _("Specify search target folder"),
			     NULL);

	folder_btn = gtk_button_new_with_label("...");
	gtk_box_pack_start(GTK_BOX(hbox), folder_btn, FALSE, FALSE, 0);

	folder_with_sub_chkbtn = gtk_check_button_new_with_label(_("Search subfolders"));
	gtk_box_pack_start(GTK_BOX(hbox), folder_with_sub_chkbtn, FALSE, FALSE, 0);

	hbox = gtk_hbox_new(FALSE, 8);
	label = gtk_label_new(_("Date newer than:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
	dategt_entry = gtk_entry_new();
	gtk_widget_set_size_request(dategt_entry, 140, -1);
	gtk_box_pack_start(GTK_BOX(hbox), dategt_entry, FALSE, FALSE, 0);
	tip = gtk_tooltips_new();
	gtk_tooltips_set_tip(tip, dategt_entry, _("Date format:\n"
						  "  2007/1/15\n"
						  "  2007/02/01 12:34"),
			     NULL);

	label = gtk_label_new(_("older than:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
	datelt_entry = gtk_entry_new();
	gtk_widget_set_size_request(datelt_entry, 140, -1);
	gtk_box_pack_start(GTK_BOX(hbox), datelt_entry, FALSE, FALSE, 0);
	tip = gtk_tooltips_new();
	gtk_tooltips_set_tip(tip, datelt_entry, _("Date format:\n"
						  "  2007/1/15\n"
						  "  2007/02/01 12:34"),
			     NULL);

	has_attach_chkbtn = gtk_check_button_new_with_label(_("Attachments"));
	gtk_box_pack_start(GTK_BOX(hbox), has_attach_chkbtn, FALSE, FALSE, 0);

	gtk_table_attach(GTK_TABLE(table), hbox, 0, 2, 5, 6, GTK_FILL, 0, 0, 0);

	g_signal_connect(G_OBJECT(dategt_entry), "activate",
			 G_CALLBACK(search_entry_activated), NULL);
	g_signal_connect(G_OBJECT(datelt_entry), "activate",
			 G_CALLBACK(search_entry_activated), NULL);

	vpaned = gtk_vpaned_new();
	gtk_box_pack_start(GTK_BOX(vbox), vpaned, TRUE, TRUE, 0);

	scrolledwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_paned_add1(GTK_PANED(vpaned), scrolledwin);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolledwin),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(scrolledwin),
					    GTK_SHADOW_IN);
	gtk_widget_set_size_request(scrolledwin, -1, 200);

	store = gtk_list_store_new(N_COLS,
				   G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING,
				   G_TYPE_STRING, G_TYPE_UINT, G_TYPE_ULONG);
	treeview = gtk_tree_view_new_with_model(GTK_TREE_MODEL(store));
	g_object_unref(store);
	gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(treeview), TRUE);
	g_signal_connect(G_OBJECT(treeview), "row-activated",
			 G_CALLBACK(row_activated), NULL);

	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	gtk_tree_selection_set_mode(selection, GTK_SELECTION_SINGLE);
	g_signal_connect(G_OBJECT(selection), "changed",
			 G_CALLBACK(selection_changed), NULL);

	gtk_container_add(GTK_CONTAINER(scrolledwin), treeview);

#define APPEND_COLUMN(label, col, width)                                \
{                                                                       \
        renderer = gtk_cell_renderer_text_new();                        \
        column = gtk_tree_view_column_new_with_attributes               \
                (label, renderer, "text", col, NULL);                   \
        gtk_tree_view_column_set_resizable(column, TRUE);               \
        if (width) {                                                    \
                gtk_tree_view_column_set_sizing                         \
                        (column, GTK_TREE_VIEW_COLUMN_FIXED);           \
                gtk_tree_view_column_set_fixed_width(column, width);    \
        }                                                               \
        gtk_tree_view_column_set_sort_column_id(column, col);           \
        gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), column);   \
}

        APPEND_COLUMN(_("Subject"), COL_SUBJECT, 240);
        APPEND_COLUMN(_("From"), COL_FROM, 180);
        APPEND_COLUMN(_("Date"), COL_DATE, 0);
        APPEND_COLUMN(_("Folder"), COL_FOLDER, 0);

	scrolledwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_paned_add2(GTK_PANED(vpaned), scrolledwin);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolledwin),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(scrolledwin),
					    GTK_SHADOW_IN);

	textview = gtk_text_view_new();
	gtk_widget_set_size_request(textview, -1, 200);
	gtk_container_add(GTK_CONTAINER(scrolledwin), textview);
        gtk_text_view_set_editable(GTK_TEXT_VIEW(textview), FALSE);
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(textview), GTK_WRAP_WORD);
	gtk_text_view_set_left_margin(GTK_TEXT_VIEW(textview), 6);
	gtk_text_view_set_right_margin(GTK_TEXT_VIEW(textview), 6);

	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
	tag = gtk_text_buffer_create_tag(buffer, "match-string",
					 "background", "blue",
					 "foreground", "white",
					  NULL);		 

	hbox = gtk_hbox_new(FALSE, 8);
	gtk_box_pack_end(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

	status_label = gtk_label_new("");
	gtk_box_pack_start(GTK_BOX(hbox), status_label, FALSE, FALSE, 0);

	hbbox = gtk_hbutton_box_new();
	gtk_button_box_set_layout(GTK_BUTTON_BOX(hbbox), GTK_BUTTONBOX_END);
	gtk_box_set_spacing(GTK_BOX(hbbox), 6);

	search_btn = gtk_button_new_from_stock(GTK_STOCK_FIND);
	GTK_WIDGET_SET_FLAGS(search_btn, GTK_CAN_DEFAULT);
	gtk_box_pack_start(GTK_BOX(hbbox), search_btn, FALSE, FALSE, 0);

	clear_btn = gtk_button_new_from_stock(GTK_STOCK_CLEAR);
	GTK_WIDGET_SET_FLAGS(clear_btn, GTK_CAN_DEFAULT);
	gtk_box_pack_start(GTK_BOX(hbbox), clear_btn, FALSE, FALSE, 0);

	close_btn = gtk_button_new_from_stock(GTK_STOCK_CLOSE);
	GTK_WIDGET_SET_FLAGS(close_btn, GTK_CAN_DEFAULT);
	gtk_box_pack_start(GTK_BOX(hbbox), close_btn, FALSE, FALSE, 0);

	g_signal_connect(G_OBJECT(folder_btn), "clicked",
			 G_CALLBACK(folder_btn_clicked), NULL);
	g_signal_connect(G_OBJECT(search_btn), "clicked",
			 G_CALLBACK(search_clicked), NULL);
	g_signal_connect(G_OBJECT(clear_btn), "clicked",
			 G_CALLBACK(clear_clicked), NULL);
	g_signal_connect(G_OBJECT(close_btn), "clicked",
			 G_CALLBACK(close_clicked), NULL);

	gtk_box_pack_end(GTK_BOX(hbox), hbbox, FALSE, FALSE, 0);

	gtk_widget_show_all(window);

	ftsearch_window.window = window;
	ftsearch_window.body_entry = body_entry;
	ftsearch_window.from_entry = from_entry;
	ftsearch_window.to_entry = to_entry;
	ftsearch_window.subject_entry = subject_entry;
	ftsearch_window.dategt_entry = dategt_entry;
	ftsearch_window.datelt_entry = datelt_entry;
	ftsearch_window.folder_entry = folder_entry;
	ftsearch_window.folder_btn = folder_btn;
	ftsearch_window.folder_with_sub_chkbtn = folder_with_sub_chkbtn;
	ftsearch_window.has_attach_chkbtn = has_attach_chkbtn;
	ftsearch_window.treeview = treeview;
	ftsearch_window.store = store;
	ftsearch_window.textview = textview;
	ftsearch_window.tag = tag;
	ftsearch_window.status_label = status_label;
	ftsearch_window.search_btn = search_btn;
	ftsearch_window.clear_btn = clear_btn;
	ftsearch_window.close_btn = close_btn;
}

static void locale_init(void)
{
	setlocale(LC_ALL, "");
	if (g_path_is_absolute(LOCALEDIR))
		bindtextdomain(PACKAGE, LOCALEDIR);
	else {
		gchar *cd, *locale_dir;
		cd = g_get_current_dir();
		locale_dir = g_strconcat(cd, G_DIR_SEPARATOR_S, LOCALEDIR,
					 NULL);
#ifdef G_OS_WIN32
		{
			gchar *tmp;
			tmp = g_locale_from_utf8
				(locale_dir, -1, NULL, NULL, NULL);
			if (tmp) {
				g_free(locale_dir);
				locale_dir = tmp;
			}
		}
#endif
		bindtextdomain(PACKAGE, locale_dir);
		g_free(locale_dir);
		g_free(cd);
	}
	bind_textdomain_codeset(PACKAGE, "UTF-8");
	textdomain(PACKAGE);
}

#ifdef G_OS_WIN32
static gchar *get_win32_special_folder_path(gint nfolder)
{
	gchar *folder = NULL;
	HRESULT hr;

	wchar_t path[MAX_PATH + 1];
	hr = SHGetFolderPathW(NULL, nfolder, NULL, 0, path);
	if (hr == S_OK)
		folder = g_utf16_to_utf8(path, -1, NULL, NULL, NULL);
	return folder;
}
#endif

static void config_dir_init(void)
{
	gchar *path;

#ifdef G_OS_WIN32
	gchar *appdata;

	appdata = get_win32_special_folder_path(CSIDL_APPDATA);
	if (appdata) {
		path = g_strconcat(appdata, G_DIR_SEPARATOR_S, "Sylph-Searcher", NULL);
		g_free(appdata);
	} else
		path = g_strconcat(get_home_dir(), G_DIR_SEPARATOR_S,
				   "Sylph-Searcher", NULL);
#else
	path = g_strconcat(get_home_dir(), G_DIR_SEPARATOR_S, ".sylph-searcher",
			   NULL);
#endif
	if (!is_dir_exist(path))
		make_dir(path);
	set_rc_dir(path);
	g_free(path);
}

static void init_console(void)
{
#ifdef G_OS_WIN32
	gint fd;
	FILE *fp;
	static gboolean init_console_done = FALSE;

	if (init_console_done)
		return;

	if (!AllocConsole()) {
		g_warning("AllocConsole() failed\n");
		return;
	}

	fd = _open_osfhandle((glong)GetStdHandle(STD_OUTPUT_HANDLE), _O_TEXT);
	_dup2(fd, 1);
	fp = _fdopen(fd, "w");
	*stdout = *fp;
	setvbuf(stdout, NULL, _IONBF, 0);
	fd = _open_osfhandle((glong)GetStdHandle(STD_ERROR_HANDLE), _O_TEXT);
	_dup2(fd, 2);
	fp = _fdopen(fd, "w");
	*stderr = *fp;
	setvbuf(stderr, NULL, _IONBF, 0);

	init_console_done = TRUE;
#endif
}

static void cleanup_console(void)
{
#ifdef G_OS_WIN32
	FreeConsole();
#endif
}

void alert_dialog(const gchar *title, const gchar *msg)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new(GTK_WINDOW(ftsearch_window.window),
					GTK_DIALOG_DESTROY_WITH_PARENT,
					GTK_MESSAGE_ERROR,
					GTK_BUTTONS_OK,
					"%s", title);
#if GTK_CHECK_VERSION(2, 6, 0)
	gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(dialog),
						 "%s", msg);
#endif
	gtk_window_present(GTK_WINDOW(ftsearch_window.window));
	gtk_widget_show(dialog);
	gtk_window_set_transient_for(GTK_WINDOW(dialog),
				     GTK_WINDOW(ftsearch_window.window));
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

int main(int argc, char *argv[])
{
	gchar *dbname, *host, *user, *pass;
	gushort port;
	gint conf_ok;

	syl_init();
	locale_init();

	prefs_common_read_config();
	account_read_config_all();
	if (folder_read_list() < 0)
		g_warning("can't read folder list");

	config_dir_init();

	conf_ok = read_config(&config);

	parse_cmdline(argc, argv, &dbname, &host, &port, &user, &pass);
	if (cmdline_has_option(argc, argv, "--debug"))
		init_console();

	gtk_set_locale();
	gtk_init(&argc, &argv);

	syl_searcher_create();

#define OVERWRITE_MEMBER(member, val) \
	if (val) { \
		g_free(config.member); \
		config.member = g_strdup(val); \
	}

	OVERWRITE_MEMBER(dbname, dbname);
	OVERWRITE_MEMBER(hostname, host);
	OVERWRITE_MEMBER(user, user);
	OVERWRITE_MEMBER(pass, pass);
	if (port > 0)
		config.port = port;

	if (conf_ok < 0) {
		if (!config.dbname)
			config.dbname = g_strdup("sylph");
#ifdef G_OS_WIN32
		if (!port)
			config.port = 25432;
#endif
		searcher_config(&config, GTK_WINDOW(ftsearch_window.window));
	}

	conn = db_connect(config.dbname, config.hostname, config.port,
			  config.user, config.pass);
	if (!conn)
		alert_dialog(_("Connection to database failed"),
			     _("Connection to database failed."));
	else if (db_check_tables(conn) < 0)
		alert_dialog(_("Checking of database failed"),
			     _("Checking of database failed."));

	gtk_main();

	db_disconnect(conn);
	//syl_cleanup();
	cleanup_console();

	return 0;
}

static void selection_changed(GtkTreeSelection *selection, gpointer data)
{
	GtkTreeModel *model;
	gulong sid = 0;
	gchar *text;
	GtkTreeIter iter;

	if (!gtk_tree_selection_get_selected(selection, &model, &iter)) {
		update_text("");
		return;
	}

	gtk_tree_model_get(model, &iter, COL_MSGSID, &sid, -1);
	if (!sid) {
		update_text("");
		return;
	}

	text = db_get_body(conn, sid);
	update_text(text);
	g_free(text);
}

#ifdef G_OS_WIN32
static gchar *win32_get_app_path(void)
{
	wchar_t *key = L"Software\\Microsoft\\Windows\\CurrentVersion\\App Paths\\sylpheed.exe";
	HKEY reg_key = NULL;
	DWORD type, nbytes;
	gchar *result = NULL;

	if (RegOpenKeyExW(HKEY_LOCAL_MACHINE, key, 0, KEY_QUERY_VALUE,
			  &reg_key) == ERROR_SUCCESS &&
	    RegQueryValueExW(reg_key, L"", 0, &type, NULL, &nbytes) == ERROR_SUCCESS &&
	    type == REG_SZ) {
		wchar_t *tmp = g_new(wchar_t, (nbytes + 1) / 2 + 1);
		RegQueryValueExW(reg_key, L"", 0, &type, (LPBYTE)tmp, &nbytes);
		tmp[nbytes / 2] = '\0';
		result = g_utf16_to_utf8(tmp, -1, NULL, NULL, NULL);
		g_free(tmp);
	}

	if (!result)
		result = g_strdup("C:\\Program Files\\Sylpheed\\sylpheed.exe");

	return result;
}
#endif

static void row_activated(GtkTreeView *treeview, GtkTreePath *path,
			  GtkTreeViewColumn *column, gpointer data)
{
	GtkTreeModel *model = GTK_TREE_MODEL(ftsearch_window.store);
	GtkTreeIter iter;
	gchar *folder_id;
	guint msgnum;
	gchar *path_id;
	gchar *argv[4];

	if (gtk_tree_model_get_iter(model, &iter, path) == FALSE)
		return;

	gtk_tree_model_get(model, &iter, COL_FOLDER, &folder_id,
			   COL_MSGNUM, &msgnum, -1);
	if (!folder_id || *folder_id == '\0')
		return;

	path_id = g_strdup_printf("%s/%u", folder_id, msgnum);
	g_print("opening '%s' on Sylpheed\n", path_id);
#ifdef G_OS_WIN32
	argv[0] = win32_get_app_path();
#else
	argv[0] = "sylpheed";
#endif
	argv[1] = "--open";
	argv[2] = path_id;
	argv[3] = NULL;
	execute_async(argv);
#ifdef G_OS_WIN32
	g_free(argv[0]);
#endif
	g_free(path_id);
}

static void search_entry_activated(GtkWidget *widget, gpointer data)
{
	gtk_button_clicked(GTK_BUTTON(ftsearch_window.search_btn));
}

static gint search_deleted(GtkWidget *widget, GdkEventAny *event,
			   gpointer data)
{
	gtk_main_quit();
	return FALSE;
}

static void folder_btn_clicked(GtkButton *button, gpointer data)
{
	FolderItem *item;
	gchar *id;

	item = folder_selector(GTK_WINDOW(ftsearch_window.window));

	if (!item || item->stype == F_VIRTUAL)
		return;

	id = folder_item_get_identifier(item);
	if (id) {
		gtk_entry_set_text(GTK_ENTRY(ftsearch_window.folder_entry), id);
		g_free(id);
	}
}

static void clear_clicked(GtkButton *button, gpointer data)
{
#if GTK_CHECK_VERSION(2, 6, 0)
	gtk_tree_sortable_set_sort_column_id
		(GTK_TREE_SORTABLE(ftsearch_window.store),
		 GTK_TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID, GTK_SORT_ASCENDING);
#endif
	gtk_list_store_clear(ftsearch_window.store);
	update_text("");
	gtk_entry_set_text(GTK_ENTRY(ftsearch_window.body_entry), "");
	gtk_entry_set_text(GTK_ENTRY(ftsearch_window.from_entry), "");
	gtk_entry_set_text(GTK_ENTRY(ftsearch_window.to_entry), "");
	gtk_entry_set_text(GTK_ENTRY(ftsearch_window.subject_entry), "");
	gtk_entry_set_text(GTK_ENTRY(ftsearch_window.dategt_entry), "");
	gtk_entry_set_text(GTK_ENTRY(ftsearch_window.datelt_entry), "");
	gtk_entry_set_text(GTK_ENTRY(ftsearch_window.folder_entry), "");
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ftsearch_window.folder_with_sub_chkbtn), FALSE);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ftsearch_window.has_attach_chkbtn), FALSE);
	gtk_label_set_text(GTK_LABEL(ftsearch_window.status_label), "");
	query_info_free(query_info);
	query_info = NULL;
}

static void search_clicked(GtkButton *button, gpointer data)
{
	QueryInfo qinfo = {NULL, NULL, NULL, NULL, NULL, NULL};
	const gchar *str;

	str = gtk_entry_get_text(GTK_ENTRY(ftsearch_window.body_entry));
	if (str && *str)
		qinfo.body = (gchar *)str;
	str = gtk_entry_get_text(GTK_ENTRY(ftsearch_window.from_entry));
	if (str && *str)
		qinfo.from = (gchar *)str;
	str = gtk_entry_get_text(GTK_ENTRY(ftsearch_window.to_entry));
	if (str && *str)
		qinfo.to = (gchar *)str;
	str = gtk_entry_get_text(GTK_ENTRY(ftsearch_window.subject_entry));
	if (str && *str)
		qinfo.subject = (gchar *)str;
	str = gtk_entry_get_text(GTK_ENTRY(ftsearch_window.dategt_entry));
	if (str && *str)
		qinfo.date_gt = (gchar *)str;
	str = gtk_entry_get_text(GTK_ENTRY(ftsearch_window.datelt_entry));
	if (str && *str)
		qinfo.date_lt = (gchar *)str;
	qinfo.res_limit = config.res_limit;
	str = gtk_entry_get_text(GTK_ENTRY(ftsearch_window.folder_entry));
	if (str && *str)
		qinfo.folder = (gchar *)str;
	if (gtk_toggle_button_get_active
		(GTK_TOGGLE_BUTTON(ftsearch_window.folder_with_sub_chkbtn)))
		qinfo.folder_with_sub = TRUE;
	if (gtk_toggle_button_get_active
		(GTK_TOGGLE_BUTTON(ftsearch_window.has_attach_chkbtn)))
		qinfo.has_attach = TRUE;

	db_query(&qinfo);
}

static void close_clicked(GtkButton *button, gpointer data)
{
	gtk_main_quit();
}

static void exit_cb(void)
{
	gtk_main_quit();
}

static void copy_cb(void)
{
	GtkWidget *widget;
	GtkClipboard *clipboard;
	GtkTextBuffer *buffer;

	widget = gtk_window_get_focus(GTK_WINDOW(ftsearch_window.window));
	if (widget == ftsearch_window.textview) {
		buffer = gtk_text_view_get_buffer
			(GTK_TEXT_VIEW(ftsearch_window.textview));
		clipboard = gtk_widget_get_clipboard(ftsearch_window.textview,
						     GDK_SELECTION_CLIPBOARD);
		gtk_text_buffer_copy_clipboard(buffer, clipboard);
	} else if (GTK_IS_EDITABLE(widget)) {
		gtk_editable_copy_clipboard(GTK_EDITABLE(widget));
	}
}

static void paste_cb(void)
{
	GtkWidget *widget;

	widget = gtk_window_get_focus(GTK_WINDOW(ftsearch_window.window));
	if (GTK_IS_EDITABLE(widget)) {
		gtk_editable_paste_clipboard(GTK_EDITABLE(widget));
	}
}

static void settings_cb(void)
{
	if (searcher_config(&config, GTK_WINDOW(ftsearch_window.window))) {
		db_disconnect(conn);
		conn = db_connect(config.dbname, config.hostname, config.port,
				  config.user, config.pass);
		if (!conn)
			alert_dialog(_("Connection to database failed"),
				     _("Connection to database failed."));
	}
}

static GtkWidget *about_dialog;

static void response_cb(GtkDialog *dialog, gint response_id, gpointer data)
{
	gtk_widget_destroy(GTK_WIDGET(dialog));
	about_dialog = NULL;
}

static void about_cb(void)
{
	GtkWidget *dialog;
	GtkWidget *vbox;
	GtkWidget *label;

	if (about_dialog) {
		gtk_window_present(GTK_WINDOW(about_dialog));
		return;
	}

	dialog = gtk_dialog_new_with_buttons(_("About"), NULL, 0,
					     GTK_STOCK_OK, GTK_RESPONSE_OK,
					     NULL);
	gtk_window_set_policy(GTK_WINDOW(dialog), FALSE, FALSE, FALSE);
	gtk_dialog_set_has_separator(GTK_DIALOG(dialog), FALSE);
	gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);

	vbox = gtk_vbox_new(FALSE, 4);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 8);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), vbox,
			   FALSE, FALSE, 0);

	label = gtk_label_new("Sylph-Searcher");
	gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
	label = gtk_label_new("version " VERSION);
	gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);
	label = gtk_label_new("Copyright (C) 2007-2009 Sylpheed Development Team");
	gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);

	g_signal_connect(G_OBJECT(dialog), "response", G_CALLBACK(response_cb),
			 NULL);

	gtk_widget_show_all(dialog);
	about_dialog = dialog;
}
