/* Sylph-Searcher - full-text search program for Sylpheed
 * Copyright (C) 2007-2008 Sylpheed Development Team
 */

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <stdlib.h>

#include "searcher-config.h"

#define ATTACH_LABEL_ENTRY(str, entry) \
{ \
	hbox = gtk_hbox_new(FALSE, 0); \
	label = gtk_label_new(str); \
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0); \
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, row, row + 1, \
			 GTK_FILL, 0, 0, 0); \
	hbox = gtk_hbox_new(FALSE, 0); \
	entry = gtk_entry_new(); \
	gtk_box_pack_start(GTK_BOX(hbox), entry, FALSE, FALSE, 0); \
	gtk_table_attach(GTK_TABLE(table), hbox, 1, 2, row, row + 1, \
			 GTK_FILL, 0, 0, 0); \
	++row; \
}

gint searcher_config(AppConfig *config, GtkWindow *parent)
{
	GtkWidget *dialog;
	GtkWidget *vbox;
	GtkWidget *frame;
	GtkWidget *table;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *dbname_entry;
	GtkWidget *host_entry;
	GtkWidget *port_entry;
	GtkWidget *user_entry;
	GtkWidget *pass_entry;
	GtkWidget *limit_entry;
	gint row = 0;
	gchar buf[11];
	gint result;

	g_return_val_if_fail(config != NULL, FALSE);

	dialog = gtk_dialog_new_with_buttons
		(_("Settings"), NULL, 0,
		 GTK_STOCK_OK, GTK_RESPONSE_OK,
		 GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		 NULL);
	gtk_window_set_position(GTK_WINDOW(dialog),
				GTK_WIN_POS_CENTER_ON_PARENT);
	gtk_window_set_transient_for(GTK_WINDOW(dialog), parent);
	gtk_window_set_policy(GTK_WINDOW(dialog), FALSE, FALSE, FALSE);
	gtk_dialog_set_has_separator(GTK_DIALOG(dialog), FALSE);
	gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);

	vbox = gtk_vbox_new(FALSE, 4);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 8);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), vbox,
			   FALSE, FALSE, 0);

	frame = gtk_frame_new(_("Database"));
	gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);

	table = gtk_table_new(5, 2, FALSE);
	gtk_table_set_col_spacings(GTK_TABLE(table), 4);
	gtk_table_set_row_spacings(GTK_TABLE(table), 4);
	gtk_container_set_border_width(GTK_CONTAINER(table), 8);
	gtk_container_add(GTK_CONTAINER(frame), table);

	ATTACH_LABEL_ENTRY(_("Database name:"), dbname_entry);
	ATTACH_LABEL_ENTRY(_("Hostname:"), host_entry);
	ATTACH_LABEL_ENTRY(_("Port:"), port_entry);
	ATTACH_LABEL_ENTRY(_("Username:"), user_entry);
	ATTACH_LABEL_ENTRY(_("Password:"), pass_entry);
	gtk_entry_set_visibility(GTK_ENTRY(pass_entry), FALSE);

	hbox = gtk_hbox_new(FALSE, 4);
	label = gtk_label_new(_("Maximum display number of result:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
	limit_entry = gtk_entry_new();
	gtk_widget_set_size_request(limit_entry, 80, -1);
	gtk_box_pack_start(GTK_BOX(hbox), limit_entry, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 0);

#define SET_TEXT(entry, text) \
	if (text) { \
		gtk_entry_set_text(GTK_ENTRY(entry), text); \
	}

	SET_TEXT(dbname_entry, config->dbname);
	SET_TEXT(host_entry, config->hostname);
	if (config->port > 0) {
		g_snprintf(buf, sizeof(buf), "%u", config->port);
		gtk_entry_set_text(GTK_ENTRY(port_entry), buf);
	}
	SET_TEXT(user_entry, config->user);
	SET_TEXT(pass_entry, config->pass);
	if (config->res_limit > 0) {
		g_snprintf(buf, sizeof(buf), "%d", config->res_limit);
		gtk_entry_set_text(GTK_ENTRY(limit_entry), buf);
	}

	gtk_widget_show_all(dialog);
	gtk_window_set_transient_for(GTK_WINDOW(dialog), parent);
	result = gtk_dialog_run(GTK_DIALOG(dialog));

#define UPDATE_MEMBER(member, entry) \
{ \
	g_free(config->member); \
	config->member = gtk_editable_get_chars(GTK_EDITABLE(entry), 0, -1); \
}

	if (result == GTK_RESPONSE_OK) {
		UPDATE_MEMBER(dbname, dbname_entry);
		UPDATE_MEMBER(hostname, host_entry);
		config->port = atoi(gtk_entry_get_text(GTK_ENTRY(port_entry)));
		UPDATE_MEMBER(user, user_entry);
		UPDATE_MEMBER(pass, pass_entry);
		config->res_limit =
			atoi(gtk_entry_get_text(GTK_ENTRY(limit_entry)));

		write_config(config);
	}

	gtk_widget_destroy(dialog);

	return (result == GTK_RESPONSE_OK);
}
