/* Sylph-Searcher - full-text search program for Sylpheed
 * Copyright (C) 2007-2009 Sylpheed Development Team
 */

#include <glib.h>
#include <glib/gi18n.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <sylph/folder.h>

#include "folder-selector.h"

static GtkWidget *window;
static GtkWidget *treeview;
static GtkTreeStore *store;

static gboolean selector_selected(GtkTreeSelection *selection,
				  GtkTreeModel *model,
				  GtkTreePath *path,
				  gboolean currently_selected,
				  gpointer data);
static void selector_tree_activated(GtkTreeView *treeview, GtkTreePath *path,
				    GtkTreeViewColumn *column, gpointer data);


static void folder_selector_create(GtkWindow *parent)
{
	GtkWidget *vbox;
	GtkWidget *scrolledwin;
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;
	GtkTreeSelection *selection;

	window = gtk_dialog_new_with_buttons
		(_("Select folder"), NULL, 0,
		 GTK_STOCK_OK, GTK_RESPONSE_OK,
		 GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL);
	gtk_window_set_position(GTK_WINDOW(window),
				GTK_WIN_POS_CENTER_ON_PARENT);
	gtk_window_set_transient_for(GTK_WINDOW(window), parent);
	gtk_window_set_policy(GTK_WINDOW(window), FALSE, TRUE, FALSE);
	gtk_dialog_set_has_separator(GTK_DIALOG(window), FALSE);
	gtk_dialog_set_default_response(GTK_DIALOG(window), GTK_RESPONSE_OK);

	vbox = gtk_vbox_new(FALSE, 4);
	gtk_widget_set_size_request(vbox, -1, 420);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), 4);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(window)->vbox), vbox,
			   FALSE, FALSE, 0);

	scrolledwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_widget_set_size_request(scrolledwin, 300, -1);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolledwin),
				       GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(scrolledwin),
					    GTK_SHADOW_IN);
	gtk_box_pack_start(GTK_BOX(vbox), scrolledwin, TRUE, TRUE, 0);

	store = gtk_tree_store_new(1, G_TYPE_STRING);

	treeview = gtk_tree_view_new_with_model(GTK_TREE_MODEL(store));
	g_object_unref(G_OBJECT(store));
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(treeview), FALSE);
	gtk_tree_view_set_rules_hint(GTK_TREE_VIEW(treeview), TRUE);
	gtk_tree_view_set_search_column(GTK_TREE_VIEW(treeview), 0);

	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
	gtk_tree_selection_set_mode(selection, GTK_SELECTION_BROWSE);
	gtk_tree_selection_set_select_function(selection, selector_selected,
					       NULL, NULL);

	g_signal_connect(G_OBJECT(treeview), "row-activated",
			 G_CALLBACK(selector_tree_activated), NULL);

	gtk_container_add(GTK_CONTAINER(scrolledwin), treeview);

	column = gtk_tree_view_column_new();
	renderer = gtk_cell_renderer_text_new();
	g_object_set(renderer, "ypad", 0, NULL);
	gtk_tree_view_column_pack_start(column, renderer, TRUE);
	gtk_tree_view_column_set_attributes(column, renderer, "text", 0, NULL);
	gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);

	gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), column);

	gtk_widget_show_all(window);
}

static gboolean traverse_func(GNode *node, gpointer data)
{
	FolderItem *item;
	GtkTreeIter iter;
	gchar *id;

	item = FOLDER_ITEM(node->data);

	if (!item || !item->path || item->stype == F_VIRTUAL)
		return FALSE;

	id = folder_item_get_identifier(item);
	if (!id)
		return FALSE;

	gtk_tree_store_append(store, &iter, NULL);
	gtk_tree_store_set(store, &iter, 0, id, -1);
	g_free(id);

	return FALSE;
}

FolderItem *folder_selector(GtkWindow *parent)
{
	FolderItem *item = NULL;
	GList *list;
	Folder *folder;
	gint result;

	folder_selector_create(parent);

	for (list = folder_get_list(); list != NULL; list = list->next) {
		folder = FOLDER(list->data);
		if (folder->node)
			g_node_traverse(folder->node, G_PRE_ORDER,
					G_TRAVERSE_ALL, -1,
					traverse_func, NULL);
	}

	result = gtk_dialog_run(GTK_DIALOG(window));

	if (result == GTK_RESPONSE_OK) {
		GtkTreeSelection *selection;
		GtkTreeIter iter;
		gchar *folder_id = NULL;

		selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
		if (gtk_tree_selection_get_selected(selection, NULL, &iter)) {
			gtk_tree_model_get(GTK_TREE_MODEL(store), &iter,
					   0, &folder_id, -1);
			item = folder_find_item_from_identifier(folder_id);
			g_free(folder_id);
		}
	}

	gtk_widget_destroy(window);
	window = treeview = NULL;
	store = NULL;

	return item;
}

static gboolean selector_selected(GtkTreeSelection *selection,
				  GtkTreeModel *model,
				  GtkTreePath *path,
				  gboolean currently_selected,
				  gpointer data)
{
	return TRUE;
}

static void selector_tree_activated(GtkTreeView *treeview, GtkTreePath *path,
				    GtkTreeViewColumn *column, gpointer data)
{
	gtk_dialog_response(GTK_DIALOG(window), GTK_RESPONSE_OK);
}
