/* Sylph-Searcher - full-text search program for Sylpheed
 * Copyright (C) 2007-2009 Sylpheed Development Team
 */

#include <gtk/gtkwindow.h>
#include <sylph/folder.h>

FolderItem *folder_selector(GtkWindow *window);
