AC_PREREQ(2.50)
AC_INIT(searcher/sylph-searcher.c)
PACKAGE=sylph-searcher

MAJOR_VERSION=1
MINOR_VERSION=2
MICRO_VERSION=0
EXTRA_VERSION=
VERSION=$MAJOR_VERSION.$MINOR_VERSION.$MICRO_VERSION$EXTRA_VERSION

AC_CANONICAL_SYSTEM

AM_INIT_AUTOMAKE($PACKAGE, $VERSION, no-defile)
dnl AC_DEFINE_UNQUOTED(PACKAGE, "$PACKAGE")
dnl AC_DEFINE_UNQUOTED(VERSION, "$VERSION")
AC_SUBST(PACKAGE)
AC_SUBST(VERSION)

AM_CONFIG_HEADER(config.h)

AC_PROG_CC
AC_PROG_INSTALL
AM_PROG_LIBTOOL

AC_MSG_CHECKING([for Win32 platform])
case "$target" in
*-darwin*)
	native_win32=no
	CFLAGS="$CFLAGS -no-cpp-precomp -fno-common"
	;;
*-*-mingw*)
	native_win32=yes
	dnl CFLAGS="$CFLAGS -mms-bitfields -mwindows"
	CFLAGS="$CFLAGS -mms-bitfields"
	;;
*)
	native_win32=no
	;;
esac

AC_MSG_RESULT([$native_win32])
AM_CONDITIONAL(NATIVE_WIN32, test "$native_win32" = "yes")

dnl Checks for libraries.
AM_PATH_GLIB_2_0(2.4.0,, AC_MSG_ERROR(Test for GLib failed. See the 'INSTALL' for help.))
AM_PATH_GTK_2_0(2.4.0,, AC_MSG_ERROR(Test for Gtk failed. See the 'INSTALL' for help.))

AC_SUBST(GTK_CFLAGS)
AC_SUBST(GTK_LIBS)

ALL_LINGUAS="ja"
GETTEXT_PACKAGE=sylph-searcher
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE,"$GETTEXT_PACKAGE", [Define text domain.])

AM_GLIB_GNU_GETTEXT
MSGFMT_OPTS="-c --statistics"

if test "x$DATADIRNAME" != "x"; then
	localedir='${prefix}/${DATADIRNAME}/locale'
else
	localedir='${datadir}/locale'
fi
AC_ARG_WITH(localedir,
	[  --with-localedir=DIR    Locale directory],
	[localedir="$withval"])
AC_SUBST(localedir)

AC_ARG_WITH(pgsql,
	    [AC_HELP_STRING([--with-pgsql=DIR],
			    [search for PostgreSQL in DIR/include and DIR/lib])],
	    [if test "x$with_pgsql" != x; then
		CPPFLAGS="$CPPFLAGS -I$with_pgsql/include"
		LDFLAGS="$LDFLAGS -L$with_pgsql/lib"
	     fi])
AC_ARG_WITH(libsylph,
	    [AC_HELP_STRING([--with-libsylph=DIR],
			    [search for LibSylph in DIR/include and DIR/lib])],
	    [if test "x$with_libsylph" != x; then
		CPPFLAGS="$CPPFLAGS -I$with_libsylph/include"
		LDFLAGS="$LDFLAGS -L$with_libsylph/lib"
	     fi])

if test "x$with_pgsql" = x && pg_config --version 2>&1 >/dev/null ; then
  AC_MSG_RESULT(pg_config found.)
  CFLAGS="$CFLAGS -I`pg_config --includedir`"
fi

AC_CHECK_LIB(sylph, syl_init,, AC_MSG_ERROR(Test for LibSylph failed.))
AC_CHECK_LIB(pq, PQconnectdb,, AC_MSG_ERROR(Test for libpq failed.))

AC_ARG_ENABLE(mecab,
	[  --disable-mecab         do not use MeCab library],
	[ac_cv_enable_mecab=$enableval], [ac_cv_enable_mecab=yes])
if test "$ac_cv_enable_mecab" = yes; then
	AC_CHECK_LIB(mecab, mecab_version,, AC_MSG_ERROR(Test for MeCab failed.))
fi

AC_OUTPUT([
Makefile
po/Makefile.in
common/version.h
common/Makefile
commands/Makefile
searcher/Makefile
sql/Makefile
])
