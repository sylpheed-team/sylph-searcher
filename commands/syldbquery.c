/* Sylph-Searcher - full-text search program for Sylpheed
 * Copyright (C) 2007 Sylpheed Development Team
 */

#include <sylph/sylmain.h>
#include <sylph/prefs_common.h>
#include <sylph/account.h>
#include <sylph/folder.h>
#include <sylph/procmsg.h>
#include <sylph/codeconv.h>

#include <libpq-fe.h>
#include <stdlib.h>

#include "common.h"

#define PG_CONN(dbinfo) ((PGconn *)dbinfo->dbdata)

DBInfo *conn;

gint db_query(const gchar *query_string)
{
	PGresult *res;
	gchar *query;
	gint i;
	gchar *val;
	gchar *sep_query_str, *esc_query_str;
	gulong sid;
	gchar *from, *to, *subject, *text;

	sep_query_str = get_wakachi_text(query_string);
	esc_query_str = sql_escape_str(conn, sep_query_str);

	query = g_strdup_printf("SELECT msg_sid, hdr_from, hdr_to, hdr_subject, body_text FROM msginfo WHERE plainto_tsquery(E'%s') @@ body_index", esc_query_str);
	g_print("query: %s\n", query);
	res = PQexec(PG_CONN(conn), query);
	g_free(query);
	g_free(esc_query_str);
	g_free(sep_query_str);

	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		db_error_message(conn, "SELECT failed");
		return -1;
	}

	g_print("%d messages found.\n\n", PQntuples(res));

	for (i = 0; i < PQntuples(res); i++) {
		val = PQgetvalue(res, i, 0);
		sid = atol(val);
		from = PQgetvalue(res, i, 1);
		to = PQgetvalue(res, i, 2);
		subject = PQgetvalue(res, i, 3);
		text = PQgetvalue(res, i, 4);

		g_print("%lu %s %s %s\n", sid, from, to, subject);
		//g_print("%s\n", text);
	}

	PQclear(res);

	return 0;
}

int main(int argc, char *argv[])
{
	gchar *query_string;
	gchar *dbname, *host, *user, *pass;
	gushort port;
	gint n;

	if (argc < 2)
		return 1;

	syl_init();

	n = parse_cmdline(argc, argv, &dbname, &host, &port, &user, &pass);
	if (argc > n)
		query_string = conv_localetodisp(argv[n], NULL);
	else
		return 1;

	prefs_common_read_config();
	account_read_config_all();

	conn = db_connect(dbname, host, port, user, pass);
	if (!conn)
		exit(1);

	db_query(query_string);
	g_free(query_string);

	db_disconnect(conn);

	//syl_cleanup();

	return 0;
}
