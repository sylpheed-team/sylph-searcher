/* Sylph-Searcher - full-text search program for Sylpheed
 * Copyright (C) 2007-2009 Sylpheed Development Team
 */

#include <sylph/sylmain.h>
#include <sylph/prefs_common.h>
#include <sylph/account.h>
#include <sylph/folder.h>
#include <sylph/procmsg.h>
#include <sylph/procheader.h>
#include <sylph/codeconv.h>
#include <sylph/utils.h>

#include <time.h>
#include <libpq-fe.h>

#include "common.h"

#define PG_CONN(dbinfo) ((PGconn *)dbinfo->dbdata)

#define PART_SEPARATOR		"------------------------------------------------------------------------"
#define PART_SEPARATOR_LEN	72
#define MAX_BODY_LEN		(128 * 7 * 1024)


static DBInfo *conn;

static gint verbose = 0;
static GSList *exclude_list = NULL;
static gint stat_ins_msginfo = 0;
static gint stat_ins_folderinfo = 0;
static gint stat_skip_msginfo = 0;
static gint stat_skip_folderinfo = 0;
static gint stat_error = 0;


static gchar *make_valid_utf8(const gchar *str)
{
	const gchar *p = str, *next;
	gchar *valid, *vp;

	vp = valid = g_malloc(strlen(str) + 1);
	while (*p != '\0') {
		if (!g_utf8_validate(p, -1, &next)) {
			if (next > p) {
				memcpy(vp, p, next - p);
				vp += (next - p);
			}
			*vp++ = '?';
			p = g_utf8_find_next_char(next, NULL);
		} else {
			memcpy(vp, p, next - p);
			vp += (next - p);
			break;
		}
	}
	*vp = '\0'; 

	return valid;
}

static gint string_append_with_escape_line(GString *text, const gchar *str)
{
	const gchar *p = str, *next;
	gchar *valid_str = NULL;
	gint error = 0; 

	if (!str)
		return 0;

	if (!g_utf8_validate(str, -1, NULL)) {
		valid_str = make_valid_utf8(str);
		p = valid_str;
		error = -1;
	}

	if (!strncmp(p, PART_SEPARATOR, PART_SEPARATOR_LEN)) {
		g_string_append(text, "- ");
		g_string_append(text, PART_SEPARATOR);
		p += PART_SEPARATOR_LEN;
	}

	while ((next = strstr(p, "\n" PART_SEPARATOR)) != NULL) {
		next++;
		g_string_append_len(text, p, next - p);
		g_string_append(text, "- ");
		g_string_append(text, PART_SEPARATOR);
		p = next + PART_SEPARATOR_LEN;
	}
	if (*p != '\0')
		g_string_append(text, p);

	if (valid_str)
		g_free(valid_str);

	return error;
}

static gboolean is_nospace_ascii_str(const gchar *s, gint len)
{
	const gchar *p = s;
	const gchar *end = s + len;

	while (p < end) {
		if (*p == ' ')
			return FALSE;
		if (*p != '\r' && *p != '\n' &&
		    (*(guchar *)p > 127 || *(guchar *)p < 32))
			return FALSE;
		++p;
	}

	return TRUE;
}

#define MIN_B64LEN	64
#define MIN_B64LINES	50

static gboolean string_truncate_base64_lines(GString *text)
{
	gint len, prev_len = 0;
	gchar *p, *end, *b64_start = NULL;
	gint b64_lines = 0;
	gboolean truncated = FALSE;

	p = text->str;
	while (*p != '\0') {
		end = strchr(p, '\n');
		if (end) {
			len = end - p;
			end++;
		} else {
			len = strlen(p);
			end = p + len;
		}

		if (len >= MIN_B64LEN && is_nospace_ascii_str(p, len) &&
		    (prev_len == 0 || prev_len == len)) {
			if (!b64_start)
				b64_start = p;
			b64_lines++;
			p = end;
			prev_len = len;
		} else {
			if (b64_lines > MIN_B64LINES) {
				gssize pos = b64_start - text->str;
				gssize elen = p - b64_start;
				gssize next = end - p;

				g_string_erase(text, pos, elen);
				p = text->str + pos + next;
				truncated = TRUE;
			} else {
				p = end;
			}

			b64_start = NULL;
			b64_lines = 0;
			prev_len = 0;
		}
	}

	if (b64_lines > MIN_B64LINES) {
		gssize pos = b64_start - text->str;
		gssize elen = p - b64_start; 

		g_string_erase(text, pos, elen);
		truncated = TRUE;
	}

	return truncated;
}

static gint string_truncate_lines(GString *text, gint maxlen)
{
	gint len = 0, llen;
	gchar *p, *end;

	if (text->len <= maxlen)
		return 0;

	string_truncate_base64_lines(text);
	if (text->len <= maxlen)
		return 1;

	p = text->str;
	while (*p != '\0') {
		end = strchr(p, '\n');
		if (!end) {
			llen = strlen(p);
			end = p + llen;
		} else {
			end++;
			llen = end - p;
		}
		if (len + llen > maxlen) {
			g_string_truncate(text, len);
			return 1;
		}

		len += llen;
		p = end;
	}

	return 1;
}

gchar *get_text_content(MsgInfo *msginfo, gint *attach_num)
{
	MimeInfo *mimeinfo;
	MimeInfo *first_textpart;
	MimeInfo *partinfo;
	FILE *fp, *outfp;
	GString *text;
	gchar *str;

	mimeinfo = procmime_scan_message(msginfo);
	if (!mimeinfo) {
		g_warning("procmime_scan_message failed: msg %u",
			  msginfo->msgnum);
		return NULL;
	}

	fp = procmsg_open_message(msginfo);
	if (!fp) {
		g_warning("procmsg_open_message failed: msg %u",
			  msginfo->msgnum);
		procmime_mimeinfo_free_all(mimeinfo);
		return NULL;
	}

	first_textpart = mimeinfo;
	while (first_textpart && first_textpart->mime_type != MIME_TEXT)
		first_textpart = procmime_mimeinfo_next(first_textpart);
	if (!first_textpart) {
		first_textpart = mimeinfo;
		while (first_textpart && first_textpart->mime_type != MIME_TEXT_HTML)
			first_textpart = procmime_mimeinfo_next(first_textpart);
	}

	text = g_string_new("");

	if (first_textpart) {
		outfp = procmime_get_text_content(first_textpart, fp, NULL);
		if (!outfp) {
			g_warning("procmime_get_text_content failed: msg %u",
				  msginfo->msgnum);
			g_string_free(text, TRUE);
			fclose(fp);
			procmime_mimeinfo_free_all(mimeinfo);
			return NULL;
		}
		str = file_read_stream_to_str(outfp);
		if (str) {
			string_append_with_escape_line(text, str);
			g_free(str);
		}
		fclose(outfp);
	}

	*attach_num = 0;

	for (partinfo = mimeinfo; partinfo != NULL;
	     partinfo = procmime_mimeinfo_next(partinfo)) {
		gchar *filename, *attname = NULL;

		if (partinfo->mime_type == MIME_MULTIPART ||
		    partinfo->mime_type == MIME_MESSAGE_RFC822)
			continue;
		if (first_textpart == partinfo)
			continue;

		filename = partinfo->filename ? partinfo->filename :
			partinfo->name ? partinfo->name : NULL;
		if (filename) {
			*attach_num = *attach_num + 1;
			attname = g_path_get_basename(filename);
		}

		if (partinfo->mime_type == MIME_TEXT ||
		    partinfo->mime_type == MIME_TEXT_HTML) {
			outfp = procmime_get_text_content(partinfo, fp, NULL);
			if (!outfp) {
				g_warning("procmime_get_text_content failed: msg %u", msginfo->msgnum);
				g_free(attname);
				fclose(fp);
				procmime_mimeinfo_free_all(mimeinfo);
				return g_string_free(text, FALSE);
			}
			str = file_read_stream_to_str(outfp);
			if (str) {
				g_string_append(text, "\n" PART_SEPARATOR "\n");
				if (attname)
					g_string_append_printf(text, "[ %s (%s %s) ]\n\n", attname, partinfo->content_type ? partinfo->content_type : "text", to_human_readable(partinfo->content_size));
				string_append_with_escape_line(text, str);
				g_free(str);
			}
			fclose(outfp);
		} else {
			if (attname) {
				g_string_append(text, "\n" PART_SEPARATOR "\n");
				g_string_append_printf(text, "[ %s (%s %s) ]\n\n", attname, partinfo->content_type ? partinfo->content_type : "application/octet-stream", to_human_readable(partinfo->content_size));
			}
		}

		g_free(attname);
	}

	fclose(fp);
	procmime_mimeinfo_free_all(mimeinfo);

	string_truncate_lines(text, MAX_BODY_LEN);

	return g_string_free(text, FALSE);
}

gint db_insert_msg(MsgInfo *msginfo)
{
	PGresult *res;
	gchar *query;
	gchar mtime_str[256], date_str[256];
	gchar *msgid;
	struct tm *lt;
	gchar *folder_id, *esc_folder_id;
	gulong sid = 0, fsid = 0;
	gint ret = 0;

	g_return_val_if_fail(msginfo != NULL, -1);

	if (!msginfo->msgid || *msginfo->msgid == '\0') {
		g_warning("message %u doesn't have Message-Id. skipping.",
			  msginfo->msgnum);
		++stat_error;
		return -1;
	}

	lt = localtime(&msginfo->mtime);
	strftime(mtime_str, sizeof(mtime_str), "%Y-%m-%d %H:%M:%S", lt);
	lt = localtime(&msginfo->date_t);
	strftime(date_str, sizeof(date_str), "%Y-%m-%d %H:%M:%S", lt);

	if (msginfo->folder)
		folder_id = folder_item_get_identifier(msginfo->folder);
	else {
		gchar *utf8path;
		utf8path = conv_filename_to_utf8(msginfo->file_path);
		folder_id = g_path_get_dirname(utf8path);
		g_free(utf8path);
	}
	if (!folder_id) {
		g_warning("can't get folder id for message %u", msginfo->msgnum);
		++stat_error;
		return -1;
	}

	if (db_start_transaction(conn) < 0) {
		exit(1);
	}

	esc_folder_id = sql_escape_str(conn, folder_id);

	msgid = sql_escape_str(conn, msginfo->msgid);
	if (db_get_sid_from_msgid(conn, msgid, &sid) < 0) {
		ret = -1;
		++stat_error;
		goto finish;
	}

	if (sid > 0) {
		if (verbose)
			g_print("message '%s' (%s/%u) already exists in msginfo. skipping.\n",
				msginfo->msgid, folder_id, msginfo->msgnum);
		++stat_skip_msginfo;
	} else {
		MsgInfo *fullinfo;
		gchar *from, *to, *cc, *newsgroups, *subject, *inreplyto;
		gchar *body_text, *esc_body_text;
		gchar *body_index_text, *esc_body_index_text;
		gint attach_num = 0;

		if (msginfo->folder)
			fullinfo = procmsg_msginfo_get_full_info(msginfo);
		else
			fullinfo = msginfo;
		if (!fullinfo) {
			ret = -1;
			++stat_error;
			goto finish;
		}
		body_text = get_text_content(fullinfo, &attach_num);
		if (!body_text) {
			if (msginfo->folder)
				procmsg_msginfo_free(fullinfo);
			ret = -1;
			++stat_error;
			goto finish;
		}
		esc_body_text = sql_escape_str(conn, body_text);
		body_index_text = get_wakachi_text(body_text);
		if (!body_index_text) {
			g_free(esc_body_text);
			g_free(body_text);
			if (msginfo->folder)
				procmsg_msginfo_free(fullinfo);
			ret = -1;
			++stat_error;
			goto finish;
		}
		esc_body_index_text = sql_escape_str(conn, body_index_text);

		from = sql_escape_str(conn, fullinfo->from);
		to = sql_escape_str(conn, fullinfo->to);
		cc = sql_escape_str(conn, fullinfo->cc);
		newsgroups = sql_escape_str(conn, fullinfo->newsgroups);
		subject = sql_escape_str(conn, fullinfo->subject);
		inreplyto = sql_escape_str(conn, fullinfo->inreplyto);

		g_print("inserting %s/%u (%s)\n",
			folder_id, msginfo->msgnum, subject);

		query = g_strdup_printf
			("INSERT INTO msginfo(file_size, file_mtime, msg_date, flags, hdr_from, hdr_to, hdr_cc, hdr_newsgroups, hdr_subject, hdr_msgid, hdr_inreplyto, hdr_references, body_text, body_index, attach_num)"
		 	" VALUES(%u, E'%s', E'%s', %u, E'%s', E'%s', E'%s', E'%s', E'%s', E'%s', E'%s', E'%s', E'%s', to_tsvector(E'%s'), %d)",
		 	msginfo->size, mtime_str, date_str, msginfo->flags.perm_flags, from, to, cc, newsgroups, subject, msgid, inreplyto, "", esc_body_text, esc_body_index_text, attach_num);

		res = PQexec(PG_CONN(conn), query);
		g_free(query);
		g_free(inreplyto);
		g_free(subject);
		g_free(newsgroups);
		g_free(cc);
		g_free(to);
		g_free(from);
		g_free(esc_body_index_text);
		g_free(body_index_text);
		g_free(esc_body_text);
		g_free(body_text);
		if (msginfo->folder)
			procmsg_msginfo_free(fullinfo);

		if (PQresultStatus(res) != PGRES_COMMAND_OK) {
			g_warning("message: %u %s %s %s", msginfo->msgnum, msginfo->subject, msginfo->from, msginfo->date);
			db_error_message(conn, "INSERT INTO msginfo failed");
			PQclear(res);
			ret = -1;
			++stat_error;
			goto finish;
		}

		PQclear(res);
		++stat_ins_msginfo;
	}

	if (db_get_sid_from_folderinfo(conn, esc_folder_id, msginfo->msgnum,
				       &fsid) < 0) {
		ret = -1;
		++stat_error;
		goto finish;
	}

	if (fsid > 0) {
		if (fsid == sid) {
			if (verbose)
				g_print("message %s/%u already exists in msg_folderinfo. skipping.\n",
					folder_id, msginfo->msgnum);
			++stat_skip_folderinfo;
			goto finish;
		}

		g_print("message %s/%u has been changed. updating.\n",
			folder_id, msginfo->msgnum);
		if (sid == 0) {
			query = g_strdup_printf("UPDATE msg_folderinfo SET msg_sid = currval('msginfo_msg_sid_seq') WHERE folder_id = E'%s' AND msgnum = %u", esc_folder_id, msginfo->msgnum);
		} else {
			query = g_strdup_printf("UPDATE msg_folderinfo SET msg_sid = %lu WHERE folder_id = E'%s' AND msgnum = %u", sid, esc_folder_id, msginfo->msgnum);
		}
	} else {
		if (sid == 0) {
			query = g_strdup_printf("INSERT INTO msg_folderinfo(msg_sid, folder_id, msgnum) VALUES(currval('msginfo_msg_sid_seq'), E'%s', %u)",
						esc_folder_id, msginfo->msgnum);
		} else {
			query = g_strdup_printf("INSERT INTO msg_folderinfo(msg_sid, folder_id, msgnum) VALUES(%lu, E'%s', %u)",
						sid, esc_folder_id, msginfo->msgnum);
		}
	}

	res = PQexec(PG_CONN(conn), query);
	g_free(query);
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		g_warning("message: %u %s %s %s", msginfo->msgnum, msginfo->subject, msginfo->from, msginfo->date);
		db_error_message(conn, "INSERT INTO msg_folderinfo failed");
		PQclear(res);
		ret = -1;
		++stat_error;
		goto finish;
	}

	PQclear(res);
	++stat_ins_folderinfo;

finish:
	if (db_end_transaction(conn) < 0) {
		ret = -1;
		++stat_error;
	}

	g_free(msgid);
	g_free(esc_folder_id);
	g_free(folder_id);

	return ret;
}

gint db_cleanup_removed_msgs(const gchar *folder_id, GSList *mlist,
			     gboolean delete_msginfo)
{
	GHashTable *table;
	gchar *esc_folder_id;
	gchar *query;
	gint i;
	PGresult *res;
	gint ret = 0;
	gint stat_del_msginfo = 0;
	gint stat_del_folderinfo = 0;

	g_return_val_if_fail(folder_id != NULL, -1);

	g_print("\ncleanup removed messages in %s ...\n", folder_id);

	table = procmsg_msg_hash_table_create(mlist);

	esc_folder_id = sql_escape_str(conn, folder_id);

	query = g_strdup_printf("SELECT msg_sid, msgnum FROM msg_folderinfo WHERE folder_id = E'%s'", esc_folder_id);
	res = PQexec(PG_CONN(conn), query);
	g_free(query);

	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		db_error_message(conn, "SELECT from msg_folderinfo failed");
		PQclear(res);
		g_free(esc_folder_id);
		if (table)
			g_hash_table_destroy(table);
		return -1;
	}

	for (i = 0; i < PQntuples(res); i++) {
		gchar *val;
		gulong sid;
		guint msgnum;

		val = PQgetvalue(res, i, 0);
		sid = atol(val);
		val = PQgetvalue(res, i, 1);
		msgnum = atoi(val);

		if (!table ||
		    !g_hash_table_lookup(table, GUINT_TO_POINTER(msgnum))) {
			g_print("message %s/%u does not exist. removing.\n",
				folder_id, msgnum);
			if (db_delete_msg_from_folderinfo(conn, esc_folder_id,
							  msgnum) < 0) {
				ret = -1;
				break;
			}
			++stat_del_folderinfo;
			if (delete_msginfo &&
			    db_is_sid_exist_in_folderinfo(conn, sid) == 0) {
				if (db_delete_msg(conn, sid) < 0) {
					ret = -1;
					break;
				}
				++stat_del_msginfo;
			}
		}
	}

	PQclear(res);
	g_free(esc_folder_id);
	if (table)
		g_hash_table_destroy(table);

	g_print("%d deleted from msginfo, and %d deleted from msg_folderinfo\n",
		stat_del_msginfo, stat_del_folderinfo);

	return ret;
}

static gboolean is_excluded(const gchar *path)
{
	gchar *base;
	GSList *cur;
	gboolean excluded = FALSE;

	base = g_path_get_basename(path);

	for (cur = exclude_list; cur != NULL; cur = cur->next) {
		if (!strcmp(base, (gchar *)cur->data)) {
			g_print("\nfolder '%s' will be excluded\n", path);
			excluded = TRUE;
			break;
		}
	}

	g_free(base);

	return excluded;
}

gint db_import_mh_folder(const gchar *path, gboolean del_nonexist,
			 gboolean recursive)
{
	gchar *path_, *p;
	const gchar *filename;
	GDir *dir;
	Folder *mh_folder;
	FolderItem *mh_item;
	GSList *mlist = NULL, *cur;
	GSList *dirlist = NULL;
	gchar *folder_id;
	gint ret = 0;

	g_return_val_if_fail(path != NULL, -1);

	if (*path == '\0')
		return -1;
	path_ = g_strdup(path);
	p = path_ + strlen(path_) - 1;
	while (p >= path_ && *p == G_DIR_SEPARATOR) {
		*p = '\0';
		--p;
	}
	if (*path_ == '\0') {
		g_free(path_);
		return -1;
	}
	if (!is_dir_exist(path_)) {
		g_free(path_);
		return -1;
	}

	folder_id = conv_filename_to_utf8(path_);

	if (exclude_list && is_excluded(folder_id)) {
		g_free(folder_id);
		g_free(path_);
		return 0;
	}

	g_print("\nimporting %s ...\n", folder_id);

	mh_folder = folder_new(F_MH, "MHFolder", path_);
	mh_item = FOLDER_ITEM(mh_folder->node->data);
	mh_item->path = g_strdup("");
	mlist = folder_item_get_msg_list(mh_item, TRUE);
	mlist = procmsg_sort_msg_list(mlist, SORT_BY_NUMBER, SORT_ASCENDING);

	stat_ins_msginfo = stat_ins_folderinfo =
		stat_skip_msginfo = stat_skip_folderinfo = stat_error = 0;

	for (cur = mlist; cur != NULL; cur = cur->next) {
		MsgInfo *msginfo = (MsgInfo *)cur->data;
		msginfo->folder = NULL;
		msginfo->file_path = g_strdup_printf("%s%c%u", path_, G_DIR_SEPARATOR, msginfo->msgnum);
		db_insert_msg(msginfo);
	}

	g_print("\n%s: %d total messages\n", folder_id, mh_item->total);
	g_print("%d inserted into msginfo (%d skipped)\n",
		stat_ins_msginfo, stat_skip_msginfo);
	g_print("%d inserted into msg_folderinfo (%d skipped)\n",
		stat_ins_folderinfo, stat_skip_folderinfo);
	g_print("%d error\n", stat_error);

	db_cleanup_removed_msgs(folder_id, mlist, del_nonexist);

	procmsg_msg_list_free(mlist);
	folder_destroy(mh_folder);
	g_free(folder_id);

	if (recursive) {
		dir = g_dir_open(path_, 0, NULL);
		if (dir) {
			while ((filename = g_dir_read_name(dir)) != NULL) {
				gchar *file;
				file = g_strconcat(path_, G_DIR_SEPARATOR_S,
						   filename, NULL);
				if (is_dir_exist(file))
					dirlist = g_slist_prepend(dirlist, file);
				else
					g_free(file);
			}
			dirlist = g_slist_reverse(dirlist);
			g_dir_close(dir);
		}

		for (cur = dirlist; cur != NULL; cur = cur->next) {
			ret = db_import_mh_folder((gchar *)cur->data,
						  del_nonexist, recursive);
			if (ret < 0)
				break;
		}

		slist_free_strings(dirlist);
		g_slist_free(dirlist);
	}

	g_free(path_);

	return ret;
}

gint db_import_folder_item(FolderItem *item, gboolean del_nonexist,
			   gboolean recursive)
{
	GSList *mlist, *cur;
	gchar *full_id;
	gint ret = 0;

	g_return_val_if_fail(item != NULL, -1);

	if (item->stype == F_VIRTUAL || item->stype == F_QUEUE ||
	    item->stype == F_TRASH)
		return 0;

	if (item->path) {
		if (exclude_list && is_excluded(item->path))
			return 0;

		full_id = folder_item_get_identifier(item);

		g_print("\nimporting %s ...\n", full_id);

		mlist = folder_item_get_msg_list(item, TRUE);
		mlist = procmsg_sort_msg_list
			(mlist, SORT_BY_NUMBER, SORT_ASCENDING);

		stat_ins_msginfo = stat_ins_folderinfo =
			stat_skip_msginfo = stat_skip_folderinfo =
			stat_error = 0;

		for (cur = mlist; cur != NULL; cur = cur->next) {
			MsgInfo *msginfo = (MsgInfo *)cur->data;
			db_insert_msg(msginfo);
		}

		g_print("\n%s: %d total messages\n", full_id, item->total);
		g_print("%d inserted into msginfo (%d skipped)\n",
			stat_ins_msginfo, stat_skip_msginfo);
		g_print("%d inserted into msg_folderinfo (%d skipped)\n",
			stat_ins_folderinfo, stat_skip_folderinfo);
		g_print("%d error\n", stat_error);

		db_cleanup_removed_msgs(full_id, mlist, del_nonexist);

		procmsg_msg_list_free(mlist);
		g_free(full_id);
	}

	if (recursive && item->node && item->node->children) {
		GNode *child = item->node->children;

		while (child) {
			ret = db_import_folder_item(FOLDER_ITEM(child->data),
						    del_nonexist, recursive);
			if (ret < 0)
				return ret;
			child = child->next;
		}
	}

	return ret;
}

gint db_import_folder(const gchar *folder_id, gboolean del_nonexist,
		      gboolean recursive)
{
	gint ret;

	g_return_val_if_fail(folder_id != NULL, -1);

	if (g_path_is_absolute(folder_id)) {
		gchar *path;

		path = conv_filename_from_utf8(folder_id);
		ret = db_import_mh_folder(path, del_nonexist, recursive);
		g_free(path);
	} else {
		FolderItem *item;

		item = folder_find_item_from_identifier(folder_id);
		if (!item) {
			g_warning("folder item '%s' not found.\n", folder_id);
			return -1;
		}
		ret = db_import_folder_item(item, del_nonexist, recursive);
	}

	return ret;
}

int main(int argc, char *argv[])
{
	Options *options;
	gchar *folder_id = NULL;
	gint n;
	gboolean del_nonexist;
	gboolean recursive;

	syl_init();

	options = g_new0(Options, 1);
	n = parse_cmdline_options(argc, argv, options);
	if (argc > n)
		folder_id = conv_localetodisp(argv[n], NULL);
	else {
		g_warning("specify target folder.");
		exit(1);
	}
	del_nonexist = !options->no_remove;
	recursive = options->recursive;
	verbose = options->verbose;
	exclude_list = options->exclude_list;

	prefs_common_read_config();
	account_read_config_all();

	if (folder_read_list() < 0) {
		g_warning("can't read folder list");
	}

	conn = db_connect(options->dbname, options->hostname, options->port,
			  options->user, options->pass);
	if (!conn)
		exit(1);
	if (db_check_tables(conn) < 0) {
		db_disconnect(conn);
		exit(1);
	}

	if (options->delete_folder) {
		if (db_delete_folder(conn, folder_id, TRUE) < 0) {
			g_warning("remove folder failed: %s", folder_id);
			db_disconnect(conn);
			exit(1);
		}
		db_disconnect(conn);
		exit(0);
	}

	db_import_folder(folder_id, del_nonexist, recursive);

	db_disconnect(conn);

	g_free(folder_id);
	//syl_cleanup();

	return 0;
}
