/* Sylph-Searcher - full-text search program for Sylpheed
 * Copyright (C) 2007-2008 Sylpheed Development Team
 */

#include <sylph/sylmain.h>
#include <sylph/prefs_common.h>
#include <sylph/account.h>
#include <sylph/folder.h>
#include <sylph/procmsg.h>
#include <sylph/procheader.h>
#include <sylph/codeconv.h>
#include <sylph/utils.h>

#include <string.h>
#include <time.h>
#include <libpq-fe.h>
#ifdef HAVE_LIBMECAB
#  include <mecab.h>
#endif

#include "common.h"

#define PG_CONN(dbinfo) ((PGconn *)dbinfo->dbdata)

#ifdef HAVE_LIBMECAB
#ifdef G_OS_WIN32
static CharSet mecab_encoding = C_SHIFT_JIS;
#else
static CharSet mecab_encoding = C_EUC_JP;
#endif
#endif /* HAVE_LIBMECAB */

gchar *sql_escape_str(DBInfo *conn, const gchar *str)
{
	gchar *esc_str;
	size_t len;

	if (!str)
		return g_strdup("");

	len = strlen(str);
	esc_str = g_malloc(len * 2 + 1);
	esc_str[0] = '\0';
	PQescapeStringConn(PG_CONN(conn), esc_str, str, len, NULL);

	return esc_str;
}

gchar *sql_escape_like_str(DBInfo *conn, const gchar *str)
{
	gchar *esc_like_str;
	gchar *esc_str;
	const gchar *p = str;
	gchar *dp;
	size_t len;

	if (!str)
		return g_strdup("");

	len = strlen(str);
	dp = esc_like_str = g_malloc(len * 2 + 1);
	while (*p) {
		if (*p == '%' || *p == '_' || *p == '\\')
			*dp++ = '\\';
		*dp++ = *p++;
	}
	*dp = '\0';
	esc_str = sql_escape_str(conn, esc_like_str);
	g_free(esc_like_str);

	return esc_str;
}

#define MAXWAKACHILEN 128

gchar *get_wakachi_text(const gchar *str)
{
#ifdef HAVE_LIBMECAB
	gchar buf[MAXWAKACHILEN + 1];
	mecab_t *mecab;
	const mecab_node_t *node;
	gchar *mecabenc_str;
	gchar *utf8_str;
	GString *string;
	gint len;

#ifdef G_OS_WIN32
	gchar *dir, *argv[2];

	dir = g_win32_locale_filename_from_utf8(get_startup_dir());
	argv[0] = "-r";
	argv[1] = g_strdup_printf("%s\\mecabrc", dir);
	mecab = mecab_new(2, argv);
	g_free(argv[1]);
	g_free(dir);
#else
	mecab = mecab_new2("");
#endif
	if (!mecab) {
		g_warning("mecab_new2 returned error");
		exit(1);
	}

	if (mecab_encoding == C_EUC_JP)
		mecabenc_str = conv_codeset_strdup(str, CS_UTF_8, CS_EUC_JP);
	else if (mecab_encoding == C_SHIFT_JIS || mecab_encoding == C_CP932)
		mecabenc_str = conv_codeset_strdup(str, CS_UTF_8, CS_CP932);
	else
		mecabenc_str = g_strdup(str);

	//node = mecab_sparse_tonode(mecab, mecabenc_str);
	node = mecab_sparse_tonode2(mecab, mecabenc_str,
				    strlen(mecabenc_str) + 1);

	string = g_string_new("");

	for (; node; node = node->next) {
		if (node->stat != MECAB_BOS_NODE &&
		    node->stat != MECAB_EOS_NODE) {
			len = MIN(MAXWAKACHILEN, node->length);
			if (node->length > MAXWAKACHILEN) {
				g_warning("wakachi: node->length(%d) too long. max: %d", node->length, MAXWAKACHILEN);
				continue;
			}

			strncpy(buf, node->surface, len);
			*(buf + len) = '\0';

			if (*buf != '\0') {
				if (string->len > 0)
					g_string_append_c(string, ' ');
				g_string_append(string, buf);
			}
		}
	}

	g_free(mecabenc_str);
	mecab_destroy(mecab);

	if (mecab_encoding == C_EUC_JP)
		utf8_str = conv_codeset_strdup(string->str, CS_EUC_JP, CS_UTF_8);
	else if (mecab_encoding == C_SHIFT_JIS || mecab_encoding == C_CP932)
		utf8_str = conv_codeset_strdup(string->str, CS_CP932, CS_UTF_8);
	else
		utf8_str = g_strdup(string->str);

	g_string_free(string, TRUE);
	return utf8_str;
#else
	return g_strdup(str);
#endif /* HAVE_LIBMECAB */
}

GSList *get_phrase_text(const gchar *str)
{
	GSList *list = NULL;
	const gchar *p = str;
	const gchar *start;
	const gchar *end;
	gchar *phrase;

	if (!str)
		return NULL;

	while ((p = strchr(p, '"'))) {
		++p;
		start = p;
		if ((end = strchr(p, '"'))) {
			if (start < end) {
				phrase = g_strstrip(g_strndup(start, end - start));
				g_print("phrase: %s\n", phrase);
				if (*phrase)
					list = g_slist_append(list, phrase);
				else
					g_free(phrase);
			}
			p = end + 1;
		} else {
			phrase = g_strstrip(g_strdup(start));
			g_print("phrase: %s\n", phrase);
			if (*phrase)
				list = g_slist_append(list, phrase);
			else
				g_free(phrase);
			break;
		}
	}

	return list;
}

static void usage(const gchar *cmdname)
{
#ifdef HAVE_LIBMECAB
	g_print("Usage: %s [-d dbname] [-h hostname] [-p port] [-U username] [-P password] [--mecab-encoding encoding] ...\n", cmdname);
#else
	g_print("Usage: %s [-d dbname] [-h hostname] [-p port] [-U username] [-P password] ...\n", cmdname);
#endif
	g_print("\n");

	g_print("Common options:\n");
	g_print("\n");
	g_print("  -d dbname                     database name\n");
	g_print("  -h hostname                   hostname of database server\n");
	g_print("  -p port                       port number of database server\n");
	g_print("  -U username                   username for database\n");
	g_print("  -P password                   password for database\n");
#ifdef HAVE_LIBMECAB
	g_print("  --mecab-encoding encoding     encoding of MeCab dictionary\n");
	g_print("                                (default: Unix: EUC-JP / Win32: Shift_JIS)\n");
#endif
	g_print("  --help                        show this message\n");
	g_print("\n");

	g_print("Options for syldbimport:\n");
	g_print("\n");
	g_print("  -n                            don't remove nonexist messages\n");
	g_print("  -r                            recursive import\n");
	g_print("  -v                            verbose output\n");
	g_print("  --exclude foldername          exclude foldername from import targets\n");
	g_print("                                (can be specified multiple times)\n");
	g_print("  --delete                      recursively delete folders from DB\n");
#ifdef G_OS_WIN32
	g_print("  --debug                       show debug console window\n");
#endif
	exit(1);
}

gint parse_cmdline(gint argc, gchar *argv[],
		   gchar **dbname, gchar **hostname, gushort *port,
		   gchar **user, gchar **pass)
{
	gint i;
	gchar *p;

	if (dbname)
		*dbname = NULL;
	if (hostname)
		*hostname = NULL;
	if (port)
		*port = 0;
	if (user)
		*user = NULL;
	if (pass)
		*pass = NULL;

	for (i = 1; i < argc; i++) {
		p = argv[i];
		if (*p == '-') {
			if (*(p + 1) == 'd') {
				if (i + 1 == argc)
					usage(argv[0]);
				*dbname = argv[i + 1];
			} else if (*(p + 1) == 'h') {
				if (i + 1 == argc)
					usage(argv[0]);
				*hostname = argv[i + 1];
			} else if (*(p + 1) == 'p') {
				if (i + 1 == argc)
					usage(argv[0]);
				*port = atoi(argv[i + 1]);
			} else if (*(p + 1) == 'U') {
				if (i + 1 == argc)
					usage(argv[0]);
				*user = argv[i + 1];
			} else if (*(p + 1) == 'P') {
				if (i + 1 == argc)
					usage(argv[0]);
				*pass = argv[i + 1];
			} else if (*(p + 1) == 'n' ||
				 *(p + 1) == 'r' ||
				 *(p + 1) == 'v')
				continue;
			else if (*(p + 1) == '-') {
#ifdef HAVE_LIBMECAB 
				if (!strncmp(p + 2, "mecab-encoding", 14)) {
					CharSet charset;

					if (i + 1 == argc)
						usage(argv[0]);
					charset = conv_get_charset_from_str(argv[i + 1]);
					if (charset != C_AUTO) {
						g_print("mecab_encoding = %s\n", argv[i + 1]);
						mecab_encoding = charset;
					}
				} else
#endif /* HAVE_LIBMECAB */
				if (!strncmp(p + 2, "help", 4))
					usage(argv[0]);
			} else
				usage(argv[0]);

			i++;
		} else
			break;
	}

	return i;
}

gint parse_cmdline_options(gint argc, gchar *argv[], Options *options)
{
	gint i;
	gchar *p;

	g_return_val_if_fail(options != NULL, -1);

	for (i = 1; i < argc; i++) {
		p = argv[i];
		if (*p == '-') {
			if (*(p + 1) == 'd') {
				if (i + 1 == argc)
					usage(argv[0]);
				options->dbname = g_strdup(argv[++i]);
			} else if (*(p + 1) == 'h') {
				if (i + 1 == argc)
					usage(argv[0]);
				options->hostname = g_strdup(argv[++i]);
			} else if (*(p + 1) == 'p') {
				if (i + 1 == argc)
					usage(argv[0]);
				options->port = atoi(argv[++i]);
			} else if (*(p + 1) == 'U') {
				if (i + 1 == argc)
					usage(argv[0]);
				options->user = g_strdup(argv[++i]);
			} else if (*(p + 1) == 'P') {
				if (i + 1 == argc)
					usage(argv[0]);
				options->pass = g_strdup(argv[++i]);
			} else if (*(p + 1) == 'n')
				options->no_remove = TRUE;
			else if (*(p + 1) == 'r')
				options->recursive = TRUE;
			else if (*(p + 1) == 'v')
				options->verbose = TRUE;
			else if (*(p + 1) == '-') {
#ifdef HAVE_LIBMECAB 
				if (!strncmp(p + 2, "mecab-encoding", 14)) {
					CharSet charset;

					if (i + 1 == argc)
						usage(argv[0]);
					++i;
					options->mecab_encoding = g_strdup(argv[i]);
					charset = conv_get_charset_from_str(argv[i]);
					if (charset != C_AUTO) {
						g_print("mecab_encoding = %s\n", argv[i]);
						mecab_encoding = charset;
					}
				} else
#endif /* HAVE_LIBMECAB */
				if (!strncmp(p + 2, "exclude", 7)) {
					if (i + 1 == argc)
						usage(argv[0]);
					options->exclude_list = g_slist_append(options->exclude_list, g_strdup(argv[++i]));
				} else if (!strncmp(p + 2, "delete", 6)) {
					options->delete_folder = TRUE;
				} else if (!strncmp(p + 2, "help", 4))
					usage(argv[0]);
			} else
				usage(argv[0]);
		} else
			break;
	}

	return i;
}
gboolean cmdline_has_option(gint argc, gchar *argv[], const gchar *opt)
{
	gint i;

	for (i = 1; i < argc; i++) {
		if (!strcmp(argv[i], opt))
			return TRUE;
	}

	return FALSE;
}

static AppConfig tmp_config;

static PrefParam param[] = {
	{"dbname", NULL, &tmp_config.dbname, P_STRING},
	{"hostname", NULL, &tmp_config.hostname, P_STRING},
	{"port", "0", &tmp_config.port, P_USHORT},
	{"user", NULL, &tmp_config.user, P_STRING},
	{"pass", NULL, &tmp_config.pass, P_STRING},
	{"res_limit", "0", &tmp_config.res_limit, P_INT},
	{NULL, NULL, NULL, P_OTHER}
};

gint read_config(AppConfig *config)
{
	gchar *path;

	g_return_val_if_fail(config != NULL, -1);

	memset(&tmp_config, 0, sizeof(tmp_config));

	path = g_strconcat(get_rc_dir(), G_DIR_SEPARATOR_S, "searcherrc", NULL);
	if (!is_file_exist(path)) {
		g_free(path);
		return -1;
	}
	prefs_read_config(param, "Searcher", path, NULL);
	g_free(path);

	*config = tmp_config;
	memset(&tmp_config, 0, sizeof(tmp_config));

	return 0;
}

gint write_config(AppConfig *config)
{
	g_return_val_if_fail(config != NULL, -1);

	tmp_config = *config;
	prefs_write_config(param, "Searcher", "searcherrc");
	memset(&tmp_config, 0, sizeof(tmp_config));

	return 0;
}

DBInfo *db_connect(const gchar *dbname, const gchar *hostname, gushort port,
		   const gchar *user, const gchar *pass)
{
	DBInfo *dbinfo;
	PGconn *conn;
	GString *conninfo;

	conninfo = g_string_new("");
	if (dbname && *dbname)
		g_string_append_printf(conninfo, "dbname = '%s' ", dbname);
	if (hostname && *hostname)
		g_string_append_printf(conninfo, "host = '%s' ", hostname);
	if (port > 0)
		g_string_append_printf(conninfo, "port = %d ", port);
	if (user && *user)
		g_string_append_printf(conninfo, "user = '%s' ", user);
	if (pass && *pass)
		g_string_append_printf(conninfo, "password = '%s' ", pass);

	conn = PQconnectdb(conninfo->str);
	dbinfo = g_new(DBInfo, 1);
	dbinfo->dbdata = conn;
	g_string_free(conninfo, TRUE);

	if (PQstatus(conn) != CONNECTION_OK) {
		db_error_message(dbinfo, "connection to database failed");
		db_disconnect(dbinfo);
		return NULL;
	}

	return dbinfo;
}

gint db_disconnect(DBInfo *conn)
{
	if (conn) {
		PQfinish(PG_CONN(conn));
		g_free(conn);
	}

	return 0;
}

gint db_check_connection(DBInfo *conn)
{
	if (!conn)
		return -1;
	if (PQstatus(PG_CONN(conn)) != CONNECTION_OK) {
		g_warning("db_check_connection: connection bad");
		return -1;
	}

	return 0;
}

gint db_check_tables(DBInfo *conn)
{
	PGresult *res;

	if (!conn)
		return -1;

	res = PQexec(PG_CONN(conn), "SELECT table_name FROM information_schema.tables WHERE table_name = 'msginfo'");
	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		db_error_message(conn, "SELECT FROM information_schema.tables failed");
		PQclear(res);
		return -1;
	}
	if (PQntuples(res) == 0) {
		g_warning("table msginfo not found");
		PQclear(res);
		return -1;
	}

	res = PQexec(PG_CONN(conn), "SELECT column_name FROM information_schema. columns WHERE table_name = 'msginfo' AND column_name = 'attach_num'");
	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		db_error_message(conn, "SELECT FROM information_schema.columns failed");
		PQclear(res);
		return -1;
	}
	if (PQntuples(res) > 0) {
		PQclear(res);
		return 0;
	}
	g_print("modifying msginfo table\n");
	if (db_exec_command(conn, "ALTER TABLE msginfo ADD attach_num INTEGER") < 0)
		return -1;

	return 0;
}

void db_error_message(DBInfo *conn, const gchar *msg)
{
	gchar *pqmsg;

	pqmsg = g_locale_to_utf8(PQerrorMessage(PG_CONN(conn)),
				 -1, NULL, NULL, NULL);
	g_warning("%s: %s", msg ? msg : "",
		  pqmsg ? pqmsg : PQerrorMessage(PG_CONN(conn)));
	g_free(pqmsg);
}

gint db_exec_command(DBInfo *conn, const gchar *query)
{
	PGresult *res;

	res = PQexec(PG_CONN(conn), query);
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		db_error_message(conn, "command failed");
		PQclear(res);
		return -1;
	}

	PQclear(res);
	return 0;
}

gint db_start_transaction(DBInfo *conn)
{
	return db_exec_command(conn, "BEGIN");
}

gint db_end_transaction(DBInfo *conn)
{
	return db_exec_command(conn, "COMMIT");
}

gint db_abort_transaction(DBInfo *conn)
{
	return db_exec_command(conn, "ROLLBACK");
}

gint db_get_sid_from_msgid(DBInfo *conn, const gchar *msgid, gulong *sid)
{
	gchar *query;
	PGresult *res;

	query = g_strdup_printf("SELECT msg_sid FROM msginfo WHERE hdr_msgid = E'%s'", msgid);
	res = PQexec(PG_CONN(conn), query);
	g_free(query);

	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		db_error_message(conn, "SELECT from msginfo failed");
		PQclear(res);
		return -1;
	}

	if (PQntuples(res) > 0) {
		gchar *val;
		val = PQgetvalue(res, 0, 0);
		*sid = atol(val);
	} else
		*sid = 0;

	PQclear(res);

	return 0;
}

gint db_get_sid_from_folderinfo(DBInfo *conn, const gchar *folder_id,
                                guint msgnum, gulong *sid)
{
	gchar *query;
	PGresult *res;

	query = g_strdup_printf("SELECT msg_sid FROM msg_folderinfo WHERE folder_id = E'%s' AND msgnum = %u", folder_id, msgnum);
	res = PQexec(PG_CONN(conn), query);
	g_free(query);

	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		db_error_message(conn, "SELECT from msg_folderinfo failed");
		PQclear(res);
		return -1;
	}

	if (PQntuples(res) > 0) {
		gchar *val;
		val = PQgetvalue(res, 0, 0);
		*sid = atol(val);
	} else
		*sid = 0;

	PQclear(res);

	return 0;
}

gint db_delete_msg_from_folderinfo(DBInfo *conn, const gchar *folder_id,
				   guint msgnum)
{
	gchar *query;
	PGresult *res;

	if (msgnum == 0)
		query = g_strdup_printf("DELETE FROM msg_folderinfo WHERE folder_id = E'%s'", folder_id);
	else
		query = g_strdup_printf("DELETE FROM msg_folderinfo WHERE folder_id = E'%s' AND msgnum = %u", folder_id, msgnum);
	g_print("query: %s\n", query);
	res = PQexec(PG_CONN(conn), query);
	g_free(query);

	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		db_error_message(conn, "DELETE FROM msg_folderinfo failed");
		PQclear(res);
		return -1;
	}

	PQclear(res);
	return 0;
}

gint db_delete_folderinfo_recursive(DBInfo *conn, const gchar *folder_id)
{
	gchar *query;
	PGresult *res;

	query = g_strdup_printf("DELETE FROM msg_folderinfo WHERE folder_id = E'%s' OR folder_id LIKE E'%s/%%'", folder_id, folder_id);
	g_print("query: %s\n", query);
	res = PQexec(PG_CONN(conn), query);
	g_free(query);

	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		db_error_message(conn, "DELETE FROM msg_folderinfo failed");
		PQclear(res);
		return -1;
	}

	PQclear(res);
	return 0;
}

gint db_is_sid_exist_in_folderinfo(DBInfo *conn, gulong sid)
{
	gchar *query;
	PGresult *res;

	query = g_strdup_printf("SELECT msg_sid FROM msg_folderinfo WHERE msg_sid = %lu LIMIT 1", sid);
	res = PQexec(PG_CONN(conn), query);
	g_free(query);

	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		db_error_message(conn, "SELECT from msg_folderinfo failed");
		PQclear(res);
		return -1;
	}

	if (PQntuples(res) > 0) {
		PQclear(res);
		return 1;
	}

	PQclear(res);
	return 0;
}

gint db_delete_msg(DBInfo *conn, gulong sid)
{
	gchar *query;
	PGresult *res;

	query = g_strdup_printf("DELETE FROM msginfo WHERE msg_sid = %lu", sid);
	res = PQexec(PG_CONN(conn), query);
	g_free(query);

	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		db_error_message(conn, "DELETE FROM msginfo failed");
		PQclear(res);
		return -1;
	}

	PQclear(res);
	return 0;
}

gint db_delete_msg_noref(DBInfo *conn)
{
	gchar *query;
	PGresult *res;

	query = g_strdup_printf("DELETE FROM msginfo WHERE msg_sid IN"
				" (SELECT msg_sid FROM msginfo EXCEPT"
				"  SELECT msg_sid FROM msg_folderinfo)");
	g_print("query: %s\n", query);
	res = PQexec(PG_CONN(conn), query);
	g_free(query);

	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		db_error_message(conn, "DELETE FROM msginfo failed");
		PQclear(res);
		return -1;
	}

	PQclear(res);
	return 0;
}

gint db_delete_folder(DBInfo *conn, const gchar *folder_id, gboolean recursive)
{
	gchar *esc_folder_id;
	gint ret = 0;

	if (db_start_transaction(conn) < 0)
		return -1;

	esc_folder_id = sql_escape_str(conn, folder_id);

	if (recursive) {
		if (db_delete_folderinfo_recursive(conn, esc_folder_id) < 0) {
			ret = -1;
			goto finish;
		}
	} else {
		if (db_delete_msg_from_folderinfo(conn, esc_folder_id, 0) < 0) {
			ret = -1;
			goto finish;
		}
	}
	if (db_delete_msg_noref(conn) < 0) {
		ret = -1;
		goto finish;
	}

finish:
	if (db_end_transaction(conn) < 0)
		ret = -1;

	if (ret == 0)
		g_print("removed %s\n", folder_id);

	g_free(esc_folder_id);
	return ret;
}

gchar *db_get_body(DBInfo *conn, gulong sid)
{
	PGresult *res;
	gchar *query;
	gchar *val, *text;

	g_return_val_if_fail(conn != NULL, NULL);

	query = g_strdup_printf("SELECT body_text FROM msginfo WHERE msg_sid = %lu", sid);
	res = PQexec(PG_CONN(conn), query);
	g_free(query);

	if (PQresultStatus(res) != PGRES_TUPLES_OK) {
		db_error_message(conn, "SELECT failed");
		return NULL;
	}

	val = PQgetvalue(res, 0, 0);
	text = g_strdup(val);

	PQclear(res);
	return text;
}
