/* Sylph-Searcher - full-text search program for Sylpheed
 * Copyright (C) 2007-2009 Sylpheed Development Team
 */

#ifndef __VERSION_H__
#define __VERSION_H__

#define PACKAGE		"sylph-searcher"
#define VERSION		"1.2.0"

#endif /* __VERSION_H__ */
