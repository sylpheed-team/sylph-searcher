sylph-searcher (1.2.0-15) unstable; urgency=medium

  * debian/control
    - Set Standards-Version: 4.5.0
    - Drop debhelper and set debhelper-compat (= 13)
  * Trim trailing whitespace.

 -- Hideki Yamane <henrich@debian.org>  Tue, 02 Jun 2020 00:13:27 +0900

sylph-searcher (1.2.0-14) unstable; urgency=medium

  * debian/compat
    - use dh12
    - set Standards-Version: 4.4.0
    - add Rules-Requires-Root: no
  * drop debian/.gitlab-ci.yml and use debian/salsa-ci.yml for CI

 -- Hideki Yamane <henrich@debian.org>  Sat, 31 Aug 2019 23:39:05 +0900

sylph-searcher (1.2.0-13) unstable; urgency=medium

  * debian/control
    - move Vcs-* to salsa.debian.org
    - set Standards-Version: 4.1.3
    - set Build-Depends: debhelper (>= 11)
    - add Depends: unidic-mecab
  * debian/compat
    - set 11
  * debian/upstream
    - add signing-key.asc to check PGP signature
  * debian/watch
    - update to use above signing-key.asc
  * debian/copyright
    - update URI to use https and fix lintian warning

 -- Hideki Yamane <henrich@debian.org>  Sun, 04 Feb 2018 13:30:17 +0900

sylph-searcher (1.2.0-12) unstable; urgency=medium

  * debian/control
    - set Standards-Version: 4.1.1
    - set Priority: optional since extra is obsolete
    - update Homepage to use https
    - set Build-Depends: debhelper (>= 10)
    - drop autotools-dev since dh10 automatically use it
  * debian/compat
    - set 10
  * debian/rules
    - drop parallel option, since it is now default
    - drop --with autotools-dev, too
    - touch AUTHORS file to avoid FTBFS with autoreconf
  * debian/clean
    - add it to remove empty AUTHORS file
  * debian/watch
    - update to version 4

 -- Hideki Yamane <henrich@debian.org>  Sat, 28 Oct 2017 17:55:22 +0900

sylph-searcher (1.2.0-11) unstable; urgency=medium

  * debian/control
    - add HAYASHI Kentaro <hayashi@clear-code.com> for Uploaders as upstream.
      Welcome!
    - set Standards-Version: 3.9.7

 -- Hideki Yamane <henrich@debian.org>  Thu, 31 Mar 2016 21:00:35 +0900

sylph-searcher (1.2.0-10) unstable; urgency=medium

  * remove debian/menu to fix "command-in-menu-file-and-desktop-file"
  * debian/control
    - set Standards-Version: 3.9.6
    - update Vcs-* field

 -- Hideki Yamane <henrich@debian.org>  Sat, 05 Dec 2015 16:40:44 +0900

sylph-searcher (1.2.0-9) unstable; urgency=low

  * debian/rules
    - Fix "update config.{sub,guess} for the AArch64 port" via
      autotools-dev (Closes: #727980)

 -- Hideki Yamane <henrich@debian.org>  Mon, 28 Oct 2013 22:13:23 +0900

sylph-searcher (1.2.0-8) unstable; urgency=low

  * debian/rules
    - add --parallel option
  * debian/control
    - set Standards-Version: 3.9.4
    - add Vcs-* field
  * remove unnecessary debian/patches

 -- Hideki Yamane <henrich@debian.org>  Fri, 12 Jul 2013 10:08:11 +0900

sylph-searcher (1.2.0-7) unstable; urgency=low

  * debian/copyright
    - update to "Machine-readable debian/copyright file 1.0"

 -- Hideki Yamane <henrich@debian.org>  Tue, 05 Jun 2012 12:59:26 +0900

sylph-searcher (1.2.0-6) unstable; urgency=low

  * debian/rules
    - convert to dh style
    - add "export DEB_BUILD_MAINT_OPTIONS := hardening=+all"
  * debian/sylph-searcher.install
    - move installation from rules
  * debian/control
    - set "Build-Depends: debhelper (>= 9)"
    - set "Standards-Version: 3.9.3"
  * debian/compat
    - set 9

 -- Hideki Yamane <henrich@debian.org>  Tue, 29 May 2012 08:56:00 +0900

sylph-searcher (1.2.0-5) unstable; urgency=low

  * debian/copyright
    - fix "missing-license-text-in-dep5-copyright" and
      "obsolete-field-in-dep5-copyright" lintian warning
  * debian/rules
    - fix "debian-rules-missing-recommended-target" lintian warning

 -- Hideki Yamane <henrich@debian.org>  Sat, 17 Sep 2011 14:45:23 +0900

sylph-searcher (1.2.0-4) unstable; urgency=low

  * debian/control
    - Standards-Version: 3.9.2
  * debian/copyright
    - update it to DEP5 style

 -- Hideki Yamane <henrich@debian.org>  Fri, 03 Jun 2011 23:07:55 +0900

sylph-searcher (1.2.0-3) unstable; urgency=low

  * debian/control
    - adjust Depends, now it depends to libpq-dev package.

 -- Hideki Yamane <henrich@debian.org>  Wed, 16 Jun 2010 23:40:31 +0900

sylph-searcher (1.2.0-2) unstable; urgency=low

  * Now really close ITP bug (Closes: #463015)
  * debian/control
    - Standards-Version: 3.8.4 with no changes
    - update my address
    - set "Build-Depends: debhelper (>= 7.0.50~)"
    - add "Uploaders: Ricardo Mones <mones@debian.org>"

 -- Hideki Yamane <henrich@debian.org>  Sun, 09 May 2010 03:12:04 +0900

sylph-searcher (1.2.0-1) unstable; urgency=low

  * New upstream release
  * Switched to source format 3.0 (quilt)
  * debian/control
    - add "Depends: {misc:Depends}"
    - remove unneccesary "\" to fix Depends line
    - change from postgresql-server-dev-8.2 | postgresql-server-dev-8.3 to
      postgresql-contrib-8.4 | postgresql-contrib-8.3, and
      from postgresql-server-dev-8.2 | postgresql-server-dev-8.3 to
      postgresql-contrib-8.4 | postgresql-contrib-8.3
    - improve descriptions, postgresql-8.2 was removed from repository
  * debian/rules
    - remove deprecated dh_desktop
  * add README.Debian to describe preparing to use sylph-searcher.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Wed, 03 Feb 2010 06:56:00 +0900

sylph-searcher (1.1.1-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - improve descriptions.
    - adjust Build-Depends: postgresql-server-dev-8.3|postgresql-server-dev-8.2,
      postgresql-contrib-8.3|postgresql-contrib-8.2
    - Standards-Version: 3.8.1
  * debian/sylph-searcher.1,copyright
    - just fix some words.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 19 Mar 2009 17:24:46 +0900

sylph-searcher (1.1.0-1) unstable; urgency=low

  * New upstream release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 04 Dec 2008 01:48:13 +0900

sylph-searcher (1.0.0-1) unstable; urgency=low

  * Initial release

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 17 Jul 2008 19:46:09 +0900
