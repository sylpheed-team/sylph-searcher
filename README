Sylph-Searcher - full-text search program for Sylpheed

Copyright (C) 2007-2009 Sylpheed Development Team

What's Sylph-Searcher
=====================

Sylph-Searcher (tentative name) is a full-text search program for messages
stored in the mailboxes of Sylpheed, or generic MH folders.
It utilizes the full-text search feature of PostgreSQL 8.3 (it will work
with 8.2, but 8.3 is recommended because the full-text search feature was
integrated at the version).

Sylph-Searcher is distributed under the BSD license. See COPYING for detail.

Sylph-Searcher requires the following programs:

GLib 2.4.0 or later (http://www.gtk.org/)
GTK+ 2.4.0 or later (http://www.gtk.org/)
MeCab 0.96 or later + mecab-ipadic (http://mecab.sourceforge.net/)
  * Don't use Mecab 0.95 or before because they have a buffer overflow bug.
  * You can install Sylph-Searcher without MeCab if you don't require
    Japanese wakachi-gaki (ex. English only use).
PostgreSQL 8.2 or later (http://www.postgresql.org/)
  * PostgreSQL 8.2 requires contrib/tsearch2
LibSylph 1.0.0 or later (http://sylpheed.sraoss.jp/)

Install
=======

First, install GLib, GTK+, MeCab, mecab-ipadic, PostgreSQL.
Please refer to each document for their installation.

1. Install tsearch2 (PostgreSQL 8.2 only)
-----------------------------------------

In the case of PostgreSQL 8.2, it is required to install tsearch2 included
in contrib of PostgreSQL. Since 8.3, tsearch2 has been built into PostgreSQL,
so the installation of tsearch2 is not required.

% cd (PostgreSQL source directory)/contrib/tsearch2
% make
% sudo make install

2. Install Sylph-Searcher
-------------------------

Next, make Sylph-Searcher and install it. Execute the following in the
source directory of Sylph-Searcher. Executable files are installed into
/usr/local/bin, and SQL scripts are installed into
/usr/local/share/sylph-searcher/sql by default.

% ./configure
% make
% sudo make install

You can change the install directory with --prefix option of configure script.
You can also specify the location of PostgreSQL and LibSylph with the
following options:

  --with-pgsql=DIR
  --with-libsylph=DIR

If you don't require Japanese wakachi-gaki (ex. English only use), you can
build Sylph-Searcher without MeCab using the following option.
In this case, Japanese text search will not work correctly.

  --disable-mecab

Next, create an user and a database for Sylph-Searcher (in this example,
database name 'sylph' with owner 'sylphuser' is used), and configure it.
Please note that the database encoding must be UTF-8.

* PostgreSQL 8.3
----------------

% su postgres
% createuser sylphuser
% createdb -O sylphuser -E UTF-8 sylph 
% exit

% psql -U sylphuser -f /usr/local/share/sylph-searcher/sql/create.sql sylph

* PostgreSQL 8.2
----------------

% su postgres
% createuser sylphuser
% createdb -O sylphuser -E UTF-8 sylph 
% psql sylph -f /usr/local/pgsql/share/contrib/tsearch2.sql
% psql sylph -c "GRANT ALL ON pg_ts_cfg TO PUBLIC"
% psql sylph -c "GRANT ALL ON pg_ts_cfgmap TO PUBLIC"
% psql sylph -c "GRANT ALL ON pg_ts_dict TO PUBLIC"
% psql sylph -c "GRANT ALL ON pg_ts_parser TO PUBLIC"
(*1)
% exit

% psql -U sylphuser -f /usr/local/share/sylph-searcher/sql/create.sql sylph

(*1) only do this additionally if the database is initialized with a locale
($LANG) other than "C":
% psql sylph -c "UPDATE pg_ts_cfg SET locale = '$LANG' where ts_name = 'default'";

How to use
==========

1. Import messages to database
------------------------------

Use syldbimport command to import messages to database. The target folder
must be specified. You can import any type of mailboxes (MH, IMAP4, News).
You can also import MH folders which are not managed by Sylpheed.

You can specify the database to connect or database name by arguments.
Refer to the next section "Command line options" for the options.

* Specify folder by identifier

% syldbimport -d sylph "#mh/Mailbox/inbox"

* Directly specify MH folder

% syldbimport -d sylph /path/to/MH/Mailbox

* Recursively import folders under "#mh/Mailbox"

% syldbimport -d sylph -r "#mh/Mailbox"

* Connect to DB server host:5431 with username 'sylphuser' and password 'pass'

% syldbimport -d sylph -h host -p 5431 -U sylphuser -P pass "#mh/Mailbox/another"

* In the case of MeCab dictionary being UTF-8

% syldbimport -d sylph -U sylphuser --mecab-encoding UTF-8 "#mh/Mailbox/foo"
% ...

WARNING: The default encoding of the MeCab dictionary assumes EUC-JP on Unix
and Shift_JIS on Windows. If the correct encoding is not specified on import,
the characters in the full-text search index will be corrupted and you will
not be able to get the correct search results.

2. Test DB query
----------------

You can perform simple full-text search by syldbquery command.
The query string will be separated into words and messages which include
all of them will match.

% syldbquery -d sylph -U sylphuser "test"

3. Execute GUI front-end
------------------------

This GUI frontend is mainly used for searching.

Configure database settings with the configuration dialog which will appear
on the first run. You can also specify them by arguments as well as the above
commands.

To execute searching, enter search words in the text entry of targets and
press Enter key, or click 'Find' button. The results are displayed on the
list view at the center. If you select a mesage in the list view, the
message body will be displayed in the text view at the below. If you
double-click the message or press Space or Enter key, it will be displayed
on Sylpheed as a new window (requires Sylpheed 2.4.2 or later).

% sylph-searcher
% sylph-searcher -d sylph -U sylphuser

Command line options
====================

syldbimport [OPTIONS]... target-folder
syldbquery [OPTIONS]... query-string
sylph-searcher [OPTIONS]...

Options for syldbimport, syldbquery and sylph-searcher:

  -d dbname			database name
  -h hostname			hostname of database server
  -p port			port number of database server
  -U username			username for database
  -P password			password for database
  --mecab-encoding encoding	encoding of MeCab dictionary
				(default: Unix: EUC-JP / Win32: Shift_JIS)

Options for syldbimport:

  -n				don't remove nonexist messages
  -r				recursive import
  -v				verbose output
  --exclude foldername		exclude foldername from import targets
				(can be specified multiple times)
  --delete			recursively delete folders from DB
